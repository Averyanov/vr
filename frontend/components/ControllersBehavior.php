<?
namespace frontend\components;

use yii\base\Behavior;

class ControllersBehavior extends Behavior
{
    public
        $menuDataProvider,
        $indexDataProvider,
        $gameDataProvider,
        $mainGameDataProvider,
        $photoDataProvider,
        $contacts,
        $newsletter,
        $camera = false,
        $pages,
        $countdown = false;


    public function getId($page)
    {
        $pages = [
            'game' => 3,
            'photo' => 5,
            'booking' => 6,
            'contacts' => 7
        ];
        return $pages[$page];
    }
}