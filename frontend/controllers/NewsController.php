<?php
namespace frontend\controllers;

use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

use frontend\components\ControllersBehavior;

use frontend\models\Game;
use frontend\models\Newsletter;
use frontend\models\Contacts;
use frontend\models\Page;
use frontend\models\News;

/**
 * Site controller
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            ControllersBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->pages = Page::find()->orderBy('sort')->all();

        $first_menu_item = new Page([
            'id' => -1,
            'url' => ''
        ]);

        $this->menuDataProvider = new ArrayDataProvider([
            'allModels' => array_merge([$first_menu_item],$this->pages),
            'pagination' => ['pageSize' => false]
        ]);

        $games = Game::find()->all();

        $this->gameDataProvider = new ArrayDataProvider([
            'allModels' => $games,
            'pagination' => ['pageSize' => false]
        ]);

        $this->mainGameDataProvider = new ArrayDataProvider([
            'allModels' => $games,
            'pagination' => ['pageSize' => 4]
        ]);

        $this->contacts = ArrayHelper::map(Contacts::find()->all(), 'name', 'value');

        $this->newsletter = new Newsletter();

        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $news_model = News::find()->orderBy('date DESC')->all();
        $page_models = array_filter($this->pages, function ($element) {return $element->id == $this->getId('contacts');});
        $page_model = array_shift($page_models);

        $newsDataProdiver = new ArrayDataProvider(['allModels' => $news_model,]);

        return $this->render('index',
            [
                'news_model' => $news_model,
                'page_model' => $page_model,
                'newsDataProvider' => $newsDataProdiver
            ]
        );
    }

    public function actionItem($id)
    {
        $model = $this->findModel($id);

        return $this->render('item', ['model' => $model]);
    }

    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
