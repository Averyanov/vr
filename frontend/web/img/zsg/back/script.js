var num
function cutImage()
{
	for(i = 0; i <= 24; i++)
	{
		el = document.getElementsByTagName('td')[i]
		document.getElementsByTagName('td')[i].style.cssText = "height:"+(el.offsetHeight-5)+"px"
	}
}
function photoBox(i)
{
	el = document.getElementById('galleryL')
	el.style.cssText = "z-index:100; top: 50%; left: 50%; margin-left:"+-((el.offsetWidth)/2)+"px; margin-top:"+-((el.offsetHeight)/2)+"px; opacity: 1; "
	el = document.getElementById('galleryL').getElementsByTagName('div')[i]
	el.style.cssText = "z-index:100; top: 50%; left: 50%; margin-left:"+-((el.offsetWidth)/2)+"px; margin-top:"+-((el.offsetHeight)/2)+"px; opacity: 1; padding: 10px 10px 5px 10px; background-color: white; border-radius: 5px;"
	coordX = (el.offsetWidth)/2
	coordY = (el.offsetHeight)/2
	el = document.getElementById('overLay')
	el.style.cssText = "height: 100%; width: 100%; background-color: Black; opacity: 0.5; z-index: 100; position: fixed; top: 0; left: 0;"
	el = document.getElementById('close')
	el.style.cssText = "top 50%; left 50%; z-index: 101; margin-left:"+(coordX-10)+"px; margin-top:"+(-coordY-10)+"px; opacity: 1;"
	num = i
}
function closeImg()
{
	el = document.getElementById('galleryL')
	el.style.cssText = "z-index: -1;"
	el = document.getElementById('overLay')
	el.style.cssText = "z-index: -1;"
	el = document.getElementById('galleryL').getElementsByTagName('div')[num]
	el.style.cssText = "z-index: -1;"
}
function set_horizontalMenu_class(li_num)
{
	document.getElementById('horizontalMenu').getElementsByTagName('ul')[0].getElementsByTagName('li')[li_num].setAttribute("class", "active")
}
function bodyOnload(li_num)
{
	set_horizontalMenu_class(li_num)
	set_BodyHeight()
	cutImage()
}
function set_BodyHeight()
{
	el = document.getElementsByTagName('html')[0]
	if(screen.height > el.offsetHeight)
	{
		el.style.cssText = "height: 100%;"
		document.getElementsByTagName('body')[0].style.cssText = "padding-bottom: 0;"
	}
}
function imgClick(i)
{
	closeImg()
	photoBox(i+1)
}