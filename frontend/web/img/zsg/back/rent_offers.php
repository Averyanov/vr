<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
		<?php
			$language = $_GET['language'];
			if(!isset($language))
			{
				$language = "ua";
			}
			else
				include "php/script.php";
		?>
		</title>
		<link href="style.css" rel="stylesheet" type="text/css" media="screen" /> 
		<script type="text/javascript" src="script.js"></script> 
	</head>
	<body onload = set_horizontalMenu_class(2)>
		<div id="container">
			<?php
				include "div/header";
			?>
			<div class="uline">
			</div>
			<?php
				include "div/horizontalMenu";
			?>
			<div id="content">
				<?php
					include "div/$language/techCh";
				?>
			</div>
			<div class="clear_both"></div>
			<div class="uline">
			</div>
		</div>
		<?php
				include "div/footer";
		?>
	</body>
	</html>