<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
		<?php
			$language = $_GET['language'];
			if(!isset($language))
			{
				$language = "ua";
			}
			else
				include "php/script.php";
		?> 
		</title>
		<link href="style.css" rel="stylesheet" type="text/css" media="screen" />  
		<script type="text/javascript" src="script.js"></script>
	</head>
	<div id="overLay" onclick="closeImg()"></div>
	<body onload = bodyOnload(1)>
		<div id="container">
			<?php
				include "div/header";
			?>
			<div class="uline">
			</div>
			<?php
				include "div/horizontalMenu";
			?>
			<div id="gallery">
				<table>
				<?php
				$i = 0;
				$folder = opendir("images/photo/prev/");
				while (($entry = readdir($folder)) != "") 
				{	
					if(preg_match('/.JPG$/',$entry))
					{
						echo "<td><img src='images/photo/prev/$entry' onclick='photoBox($i)'></td>";
						++$i;
					}
				}
				$folder = closedir($folder);
				?>
				</table>
			</div>
			<div id="galleryL">
			<?php
				$i = 0;
				$folder = opendir("images/photo/photo/");
				while (($entry = readdir($folder)) != "") 
				{	
					if(preg_match('/.JPG$/',$entry))
					{
						echo "<div class='inherit' onclick='imgClick($i)'><img src='images/photo/photo/$entry'></div>";
						++$i;
					}
				}
				$folder = closedir($folder);
			?>
				<div class="inherit" id="close" onclick="closeImg()"></div>
			</div>
		</div>
		<?php
				include "div/footer";
		?>
	</body>
	</html>
