<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
		<?php
			$language = $_GET['language'];
			if(!isset($language))
			{
				$language = "ua";
			}
			else
				include "php/script.php";
		?> 
		</title>
		<link href="style.css" rel="stylesheet" type="text/css" media="screen" /> 
		<script type="text/javascript" src="script.js"></script> 
	</head>
	<body onload = bodyOnload(3)>
		<div id="container">
			<?php
				include "div/header";
			?>
			<div class="uline">
			</div>

			<?php
				include "div/horizontalMenu";
			?>
			<div class="clear_both"></div>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5101.677886641665!2d28.649196468880756!3d50.257592254459844!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x472c64ec6c3e8e4b%3A0xea194f5c4df93b6!2z0LLRg9C7LiA4LdCz0L4g0JHQtdGA0LXQt9C90Y8sIDEzLCDQltC40YLQvtC80LjRgCwg0JbQuNGC0L7QvNC40YDRgdGM0LrQsCDQvtCx0LvQsNGB0YLRjA!5e0!3m2!1suk!2sua!4v1410252195727" width="800" height="600" frameborder="0" style="border:0"></iframe>
			<div class="uline">
			</div>

		</div>
		<?php
				include "div/footer";
		?>
	</body>
	</html>