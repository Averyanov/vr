<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
		<?php
			$language = $_GET['language'];
			if(!isset($language))
			{
				$language = "ua";
				echo "Про бізнес-центр";
			}
			else
				include "php/script.php";
		?> 
		</title>
		<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
		<script type="text/javascript" src="plugin/stepcarousel.js"></script>
		<script type="text/javascript" src="script.js"></script>
	</head>
	<body onload = bodyOnload(0)>
	<script type="text/javascript">
		stepcarousel.setup({
	   galleryid: 'gallery', 
	   beltclass: 'belt', 
	   panelclass: 'panel', 
	   autostep: {enable:true, moveby:1, pause:3000}, 
	   panelbehavior: {speed:1000, wraparound:true, persist:true},
	   defaultbuttons: {enable: false, moveby: 1, leftnav: ['http://siteurl/left.gif', -5, 80], rightnav: ['http://siteurl/right.gif', -20, 80]},
	   statusvars: ['statusA', 'statusB', 'statusC'], 
	   contenttype: ['inline']
	})
</script>
		<div id="container">
			<?php
				include "div/header";
			?>
			<div class="uline">
			</div>
			<?php
				include "div/horizontalMenu";
			?>
			<div id="gallery" class="stepcarousel">
				<div class="belt">
				<?php
				$folder = opendir("images/slider");
				while (($entry = readdir($folder)) != "") 
				{	
					if(preg_match('/.JPG$/',$entry))
					{
						echo "<div class = 'panel'><img src='images/slider/$entry'/></div>";
					}
				}
				$folder = closedir($folder);
				?>
				</div>
			</div>
			<div class="uline">
			</div>
			<div id="content">			
				<div id="blurb">
					<?php
						echo "<div id = \"photos\" onclick = \"location.href = 'photo.php?language=$language'\"></div>";
					?>
					<style type = "text/css">
					#office
					{
						<?php
							echo "background: url(images/$language/Arenda_img.jpg) no-repeat;";
						?>
					}
					</style>
					<?php
						echo "<div id = \"office\" onclick = \"location.href = 'rent_offers.php?language=$language'\"></div>";
					?>
					<div class = "clear_both"></div>
				</div>
				<?php
					include "div/$language/text";
				?>
				<div class = "clear_both"></div>
			</div>
			<div class="uline">
			</div>
		</div>
		<?php
				include "div/footer";
		?>
	</body>
</html>
