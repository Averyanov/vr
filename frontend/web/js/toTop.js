/**
 * Created by sebulba on 08.11.16.
 */
document.addEventListener('DOMContentLoaded', function(){
    window.onscroll = function () {
        var scrolled = window.pageYOffset || document.documentElement.scrollTop;
        toTop.style.display = scrolled > 100
            ? 'block'
            : 'none';
    };

    toTop.addEventListener('click', function (event) {
        event.preventDefault();
        $("html, body").animate({scrollTop: 0},500);
    });
});
