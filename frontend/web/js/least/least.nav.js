/**
 * Created by sebulba on 11.05.16.
 */
var direct = 0;//default direct for slide
classSliderItem = function($this)
{
    this.item = $this;
    this.list = $.makeArray($('ul#gallery>li:not(#fullPreview)'));
    this.length = Object.keys(this.list).length;
};

classSliderItem.prototype.slideToItem = function(direct)
{
    currentImgSrc = $(this.item).attr('src');
    nextPosition = $(this.list).find('a[href="'+currentImgSrc+'"]').parent().index();//img.list ignore first li#fullPreview

    if(nextPosition == this.length && direct != '-2')//if end of list ->  move to first img, but not for prev button
        $(this.list).find('a').eq(0).trigger('click');
    else
        $(this.list).find('a').eq(nextPosition+parseInt(direct)).trigger('click');

    return this;
};
$('#gallery li a').on('click', function()
{
    $('#galleryNav').addClass('active');
});
$('body').on('click','figure.close',function()
{
    $('#galleryNav').removeClass('active');
});
$('.gallery').on('click', 'img, nav', function()
{
    if($(this).attr('data-direct'))
        direct = $(this).attr('data-direct');

    img = new classSliderItem($('#fullPreview img')).slideToItem(direct);
});
