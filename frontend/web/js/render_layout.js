var layout = function ()
{
    const origin_background_width = 2554,
        end_window_height = 351,
        corner_height = 286,
        outer_corner_width = 338,
        inner_wrap_width = 1882,
        ref_width = 1024,
        ref_height = 681;

    var init = function ()
    {
        list = menu.getElementsByTagName('li');
        content_list = content.getElementsByClassName('content-item');
        prev_ts = 0;
        min_time_interval = 250;

        rel_sample_rate_x = window.innerWidth / ref_width;
        rel_sample_rate_y = window.innerHeight / ref_height;

        proportion = document.body.clientWidth / origin_background_width;

        items_proportions = {
            'end_window' : {
                'width' : origin_background_width,
                'height' : end_window_height
            },
            'outer_corner_top' : {
                'width' : outer_corner_width,
                'height' : corner_height,
                'top' : end_window_height
            },
            'side' : {
                'width' : 338,
                'height' : function()
                {
                    return (window.innerHeight - (end_window_height + corner_height) * 2 * proportion) / proportion + 12;
                }(),
                'top' : 635
            },
            'outer_corner_bottom' : {
                'width' : outer_corner_width,
                'height' : corner_height,
                'bottom' : end_window_height
            },
            'inner_wrap' : {
                'width' : inner_wrap_width,
                'height' : function ()
                {
                    return (window.innerHeight - outer_corner_width * 2 * proportion) / proportion -20;
                }(),
                'top' : end_window_height,
                'left' : outer_corner_width
            },
            'inner_corner_top' : {
                'width': 214,
                'height': corner_height
            },
            'inner_corner_bottom' : {
                'width' : 214,
                'height' : corner_height
            },
            'chaser' : {
                'width' : 369,
                'height' : 50,
                'left' : function () {
                    return (inner_wrap_width - 369) / 2;
                }()
            }

        };
        items_list = document.getElementsByClassName('positioned');

        // if(location.search.indexOf('img_back') == -1)
            for( i in items_list )
                if(typeof items_list[i] == 'object')
                    setPosition(items_list[i]);

        renderMenu();
    };

    var setPosition = function (item)
    {
        params = items_proportions[item.getAttribute('params-name')];

        for(key in params)
            if(typeof params[key] != 'function')
            {
                resized_param = Math.floor(params[key] * proportion);
                item.style.setProperty(key, resized_param+'px');
            }
    };
    
    var getAxisModul = function (mouse_position, axis_length)
    {
        return (1 - 2 * mouse_position / axis_length) * 5;
    };
    
    var getPositionModul = function (axis_mod) {
        return axis_mod * 5;
    };

    var transformWindow = function(x, y, width, height)
    {
        var container = document.getElementById('container'),
            x_mod = getAxisModul(x, width),
            y_mod = -getAxisModul(y, height),
            angle = function (x_mod, y_mod)
            {
                return (Math.abs(x_mod) > Math.abs(y_mod))
                    ? Math.abs(x_mod)
                    : Math.abs(y_mod);
            },
            left_position = getPositionModul(x_mod) * 5 * Math.ceil(rel_sample_rate_x),
            top_position = getPositionModul(y_mod) * -1 * 3 * rel_sample_rate_y;

        container
            .style
            .transform = 'scale(1.2) perspective(500px) rotate3d('+ y_mod +','+ x_mod +',0,'+ angle(x_mod, y_mod) +'deg)';

        container.style.top = top_position + 'px';
        container.style.left = left_position + 'px';
    };

    var moveBackground = function (x, y, item, speed, init_position)
    {
        x_position = rel_sample_rate_x * x / speed + init_position.x;
        y_position = -rel_sample_rate_y * y / speed + init_position.y ;

        item.style.backgroundPosition = x_position +'px '+ y_position +'px';
    };

    var renderMenu = function ()
    {
        for(i in list)
            if(typeof list[i] == 'object')
                list[i].setAttribute('data-seq-num', i);
    };

    var scrollMenu = function (direction)
    {
        var current_position,
            content_item_visible;
        for(i in list)
            if(typeof list[i] == 'object')
            {
                current_position = (list[i].getAttribute('data-seq-num') * 1 + direction + list.length) % list.length;

                list[i].setAttribute('data-seq-num', current_position);

                content_item_visible = (current_position == 2);

                content_list[i].setAttribute('data-visible', content_item_visible);
            }
        content.style.top = '0';
    };

    var scrollContent = function (direction)
    {
        var current_position = parseInt(window.getComputedStyle(content).top),
            next_position = current_position + 50 * -direction;

        if(
            ((Math.abs(next_position) < parseInt(window.getComputedStyle(content).height) - 200) || (direction == -1))
            && (next_position <= 0)
        )
            content.style.top = next_position + 'px';
    };

    var locateDescription = function (item, event)
    {
        var desc = item.getElementsByClassName('desc-wrap')[0],
            desc_style = window.getComputedStyle(desc),
            content_style = window.getComputedStyle(content),
            inner_style = window.getComputedStyle(inner_wrap),
            item_style = window.getComputedStyle(item),
            width_condition = (parseInt(desc_style.width) + event.x) > (parseInt(content_style.width) * 1.2 + parseInt(content_style.left)),
            height_condition = (parseInt(desc_style.height) * 1.2 + event.y + parseInt(item_style.height) * 1.2) > (parseInt(inner_style.height) * 1.2 + parseInt(inner_style.top));

        desc.style.top = '';
        desc.style.left = '';
        desc.style.right = '';

        if(width_condition && height_condition)
        {
            desc.style.right = parseInt(item_style.width) + 'px';
            desc.style.top = '0';
        }
        else
        {
            if(width_condition)
                desc.style.right = '0';

            if(height_condition)
            {
                desc.style.top = '0';
                desc.style.right = '';
                desc.style.left = parseInt(item_style.width) + 'px';
            }
        }
    };

    var photoBox = function () {
        var current_photo,
            next_photo;

        var openPhoto = function (photo) {
            current_photo = photo;
            renderBox(photo.getAttribute('target-link'), 'some alt');
        };

        var slidePhoto = function (direction) {
            setTimeout(function(){
                closeBox(false)
            });

            if(direction > 0)
                next_photo = current_photo.parentElement.nextSibling
                    ? current_photo.parentElement.nextSibling.firstChild
                    : gallery.firstChild.firstChild;
            else
                next_photo = current_photo.parentElement.previousSibling
                    ? current_photo.parentElement.previousSibling.firstChild
                    : gallery.lastChild.firstChild;

            console.log(next_photo);

            setTimeout(function(){
                openPhoto(next_photo)
            }, 300);
        };

        var renderBox = function (src, alt) {
            photo_box.className = 'photo-box enable';

            var image = new Image(),
                img_tag = document.createElement("img"),
                bottom_border = photo_box.getElementsByClassName('bottom')[0];

            image.onload = function() {
                img_tag.alt = alt;
                img_tag.src = src;

                if(window.innerHeight <= this.height || window.innerWidth <= this.width)
                {
                    var scalable_param = (this.width/window.innerWidth > this.height/window.innerHeight)
                        ? 'width'
                        : 'height',
                        probably_params = ['width', 'height'],
                        window_params = {
                            'width': window.innerWidth,
                            'height': window.innerHeight
                        };

                    for(name in probably_params)
                        if(probably_params[name] == scalable_param)
                            img_tag[scalable_param] = window_params[scalable_param] * .8;
                        else
                            img_tag[name] = img_tag[scalable_param] / this[scalable_param] * this[name];

                }

                var pb_top = (window.innerHeight - img_tag.height)/2 - 29, // 29 height of photo box border
                    pb_left = (window.innerWidth - img_tag.width)/2;

                photo_box.style.top = pb_top + 'px';
                photo_box.style.left = pb_left + 'px';

                photo_shadow.style.display = 'block';
            };

            image.src = src;

            photo_box.insertBefore(img_tag, bottom_border);

            img_tag.addEventListener('click', function () {
                photoBox.slide(1);
            })
        };

        var closeBox = function (is_permanent) {
            var img = photo_box.getElementsByTagName('img')[0];

            setTimeout(function(){
                photo_box.style.left = '-2000px'
            });

            setTimeout(function(){
                photo_box.removeChild(img);
            }, 200);

            if(is_permanent)
                photo_shadow.style.display = 'none';

        };

        return {
            open: openPhoto,
            slide: slidePhoto,
            close: closeBox
        }
    }();

    return{
        init: init,
        transfrom: transformWindow,
        moveBack: moveBackground,
        scrollMenu: scrollMenu,
        scrollContent: scrollContent,
        locDesc: locateDescription,
        photoBox: photoBox,
    };
}();

document.addEventListener('DOMContentLoaded', function()
{
    layout.init();
    document.body.onmousemove = function (event)
    {
        if(location.search != '?disable_transform')
        {
            layout.transfrom(event.clientX, event.clientY, window.innerWidth, window.innerHeight);
            layout.moveBack(event.clientX, event.clientY, document.body, 50, {'x' : -60, 'y' : 0});
        }
    };

    document.addEventListener('wheel', function (event)
    {
        if(Math.abs(event.timeStamp - prev_ts) > min_time_interval)
        {
            prev_ts = event.timeStamp;

            var delta = event.deltaY || event.detail || event.wheelDelta;

            var direction = (delta > 0)
                ? 1
                : -1;

            if(
                isParentNode('inner-wrap', event.srcElement)
                || event.srcElement == inner_wrap
            )
                layout.scrollContent(direction);
            else
                layout.scrollMenu(direction);
        }
    });

    var game_list = document.getElementsByClassName('game-item');

    for(i in game_list)
    {
        if(typeof game_list[i] == 'object')
            game_list[i].addEventListener('mouseover', function (event) {
                layout.locDesc(this, event)
            })
    }

    var photo_list = gallery.getElementsByTagName('img');

    for(i=0; i < photo_list.length; i++){
        photo_list[i].addEventListener('click', function(){
            layout.photoBox.open(this);
        })
    }

    var nav_button_list = photo_box.getElementsByClassName('nav');

    i = 0;
    while(typeof nav_button_list[i] == 'object'){
        nav_button_list[i].addEventListener('click', function () {
            layout.photoBox.slide(this.getAttribute('data-direction'))
        });
        ++i;
    }

    photo_shadow.addEventListener('click', function () {
        layout.photoBox.close(true);
    })

});

function isParentNode(parentName, childObj)
{
    var testObj = childObj.parentNode;
    var test_obj_class = testObj.getAttribute('class')
        ? testObj.getAttribute('class')
        : '';


    while(test_obj_class.indexOf(parentName) == -1)
    {
        if(testObj.tagName != 'HTML')
        {
            testObj = testObj.parentNode;
            test_obj_class = testObj.getAttribute('class')
                ? testObj.getAttribute('class')
                : '';
        }
        else
            return false;
    }
    return true;
}