var time_struct = function ()
{
    var prepareControl_element_array = function (parent_variable_attr, variable_attr) {
        return [
            {
                tag: 'a',
                attr: {
                    href: '#',
                    class: 'arrow-up fa fa-caret-square-o-up',
                    'data-parent': parent_variable_attr,
                    'data-inner': variable_attr,
                    'data-direction': 'increment'
                }
            },
            {
                tag: 'input',
                attr: {
                    type: 'text',
                    class: 'time-input',
                    'data-parent': parent_variable_attr,
                    'data-inner': variable_attr
                }
            },
            {
                tag: 'a',
                attr: {
                    href: '#',
                    class: 'arrow-down fa fa-caret-square-o-down',
                    'data-parent': parent_variable_attr,
                    'data-inner': variable_attr,
                    'data-direction': 'decrement'
                }
            }
        ]

    };

    var struct = [];

    ['begin', 'end'].forEach(function (variable_attr) {
        struct.push({
            tag: 'div',
            attr: {
                class: variable_attr
            },
            children: function () {
                var children = [],
                    parent_variable_attr = variable_attr;

                ['hours', 'mins'].forEach(function (variable_attr) {
                    children.push({
                        tag: 'div',
                        attr: {
                            class: variable_attr
                        },
                        children: prepareControl_element_array(parent_variable_attr, variable_attr)
                    })
                });

                return children;
            }()
        })
    });

    struct.push(
        {
            tag: 'div',
            attr: {
                id: 'total_wrap',
                class: 'time-total'
            },
            children: [
                {tag: 'span', attr: {class: 'caption'}},
                {tag: 'span', attr: {id: 'total_value', class: 'value'}}
            ]

        },
        {
            tag: 'div',
            attr: {
                id: 'cost_wrap',
                class: 'cost-total'
            },
            children: [
                {tag: 'span', attr: {class: 'caption'}},
                {tag: 'span', attr: {id: 'cost_value', class: 'value'}}
            ]
        },
        {
            tag: 'div',
            attr: {
                class: 'button-wrap'
            },
            children: [
                {
                    tag: 'button',
                    attr: {
                        id: 'time_picker_success',
                        class: 'btn btn-success ok'
                    },
                    children: [
                        {tag: 'TextNode', text: 'OK'}
                    ]
                },
                {
                    tag: 'button',
                    attr: {
                        id: 'time_picker_cancel',
                        class: 'btn btn-inverse cancel'
                    },
                    children: [
                        {tag: 'TextNode', text: 'Відміна'}
                    ]
                }
            ]
        }
    );

    return struct;
}();