var to_unix_standard = function (t_stamp) {return Math.floor(t_stamp / 1000)};

var some;

var beginnning_of_current_day_time_stamp,
    selected_time_stamp,
    date_picker_event;

var prev_ts = 0,
    min_mouse_sensitive = 80,
    mess = {
        hours: 'Час початку не може бути пізніше часу закінчення',
        mins: {
            min_duration: 'Мінімальна тривалість сеансу - 15 хв',
            mod: ''
        },
        overflow: {
            begin: 'Сеанс не може початись раніше 10:00',
            end: 'Після 22:00 сеанс може тривати лише за особистою домовленістю'
        }
    };
    // price_list = {
    //     discount: {
    //         15: 25,
    //         30: 50,
    //         45: 75,
    //         60: 80
    //     },
    //     usual: {
    //         15: 40,
    //         30: 60,
    //         45: 90,
    //         60: 100
    //     }
    // };

var options = {
    dataPicker: {
        weekStart: 1,
        language: 'uk',
        todayHighlight: true
    },
    modal: {
        show: false
    },
    tooltip: function (session) {
        return {title: session.begin + ' - ' + session.end}
    },
    popover: {
        title: 'Встановіть час',
        placement:'top'
    }
};

var popoverToMouse = function(event){
    return {
        left: event.pageX - $('.popover').width() / 2 + 'px',
        top: event.pageY - $('.popover').height() - 10 + 'px'
    };
};

var reserve_callback = function (data)
{
    if (data.success) renderDay(date_picker_event);
};

$(document).ready(function ()
{
    $('#modal').modal(options.modal)
        .on('hide', function () {
            $('.station-body').popover('destroy')
        });

    $('#sandbox-container div').datepicker(options.dataPicker)
        .on('changeDate', function(event)
        {
            date_picker_event = event;
            beginnning_of_current_day_time_stamp = to_unix_standard(new Date().setHours(0,0,0,0));
            selected_time_stamp = to_unix_standard(event.date.getTime());

            if(beginnning_of_current_day_time_stamp <= selected_time_stamp)// ? :
            {
                renderDay(date_picker_event);
            }
            else
                alert('Вибраний Вами день минув');

        });
});

function renderDay(event) {
    // $('.popover').remove();
    var day = event.date.getDay();
    $.ajax({
        method: 'post',
        url: '/booking/show-day',
        data: {
            current: beginnning_of_current_day_time_stamp,
            selected: selected_time_stamp
        }
    })
        .done(function (data) {
            if(data.success)
            {
                $('#modal').modal();
                $('#modal_body')[0].innerHTML = data.resp.days;
                $('.station-session').each(function () {
                    new Session(this);
                    $(this).click(function (event) {
                        event.preventDefault();
                        return false;
                    })
                });
                $('.station-body')
                    .popover(options.popover)
                    .click(function (event) {
                        var station_name = event.target.parentNode.dataset.key;
                        var time_picker = new TimePicker(
                                time_struct,
                                $('.popover').find('.popover-content')[0],
                                mess,
                                data.resp.price_list,
                                day,
                                station_name)
                            .init(event)
                            .addListeners({
                                send:
                                {
                                    url: '/booking/reserve',
                                    data:
                                    {
                                        bocdts: beginnning_of_current_day_time_stamp,
                                        sts: selected_time_stamp
                                    },
                                    done: reserve_callback
                                }
                            });

                        $('.popover').css(popoverToMouse(event));
                    })
            }
        })
}