var TimePicker = function (struct, parent, messages, price_list, day, station_name) {

    this.popover_element = parent;

    // if (this.popover_element.className.indexOf('rendered') == -1)
    $(this.popover_element).empty();
    this.render(struct, parent);

    this.state;
    this.day = day;
    this.station_name = station_name;

    this.messages = messages;
    this.price_list = price_list;

    this.input = function (owner) {

        var b = getBorderNodeList.call(owner.popover_element, 'begin'),
            e = getBorderNodeList.call(owner.popover_element, 'end'),
            showTotal = function (diff) {

                var hours_value = Math.floor(diff / 60)
                    ? Math.floor(diff / 60)
                    : '';

                var hours = getTermination(hours_value);

                var mins = diff % 60
                    ? diff % 60 + ' хвилин'
                    : '';

                total_value.innerHTML = hours + mins;

            },
            fill = function ()
            {
                for (k in [b, e])
                    for (u in [b, e][k])
                        [b, e][k][u]
                            .setCustomTooltip(owner.state.title)
                            .value = owner.state.values
                                [
                                    [b, e][k][u].dataset.parent
                                ]
                                [
                                    [b, e][k][u].dataset.inner
                                ];

                showTotal(owner.state.getDiff());
                owner.calcPrice();
            };

        return {
            begin: b,
            end: e,
            fill: fill
        }
    }(this);

    State = function(values)
    {
        for(value in values)
            this[value] = typeof values[value] === 'object'
                ? new State(values[value])
                : values[value]

    };

    min_diff = {
        mins: 15,
        hours: 60
    };

    diff = function ()
    {
        var hours_int_diff = this.values.end.hours - this.values.begin.hours,
            mins_int_diff = this.values.end.mins - this.values.begin.mins;

        return hours_int_diff >= 1
            ? hours_int_diff * 60 + mins_int_diff
            : mins_int_diff;
    };

    return this;
};

TimePicker.prototype.setValue = function (target, additional_direction)
{
    var changable_item = {
            parent : target.dataset.parent,
            inner: target.dataset.inner
        },
        direction = additional_direction || target.dataset.direction;

    this.calcValue(changable_item, direction);

    return this;
};

TimePicker.prototype.calcValue = function (item, direction)
{
    var tmp_state = new State({
        values: this.state.values,
        min_diff : min_diff,
        getDiff : diff,
        mess : this.messages
    });


    this.scenario[direction][item.parent][item.inner].call(tmp_state);
    this.state = overflow_check.call(tmp_state.values) ? tmp_state : this.state;
    this.state.title = tmp_state.title;

    delete tmp_state;

    this.input.fill();

    return this;
};

TimePicker.prototype.calcPrice = function ()
{
    var diff = this.state.getDiff(),
        price_plan = ((this.day == 6 || this.day == 0) || this.state.values.begin.hours >= 17)
            ? 'usual'
            : 'discount',
        price = diff <= 60
            ? this.price_list[price_plan][diff]
            : diff / 60 * this.price_list[price_plan][60];

    cost_value.innerHTML = price + ' грн';
};

TimePicker.prototype.scenario = {
    increment: {
        begin: {
            hours: function ()
            {
                this.values.end.hours += (this.getDiff() <= this.min_diff.hours);
                ++ this.values.begin.hours;

                this.title = interval_check.call(
                    this,
                    this.mess.hours,
                    this.getDiff() <= this.min_diff.hours,
                    {border: 'end', unit: 'hours'}
                );
            },
            mins: function ()
            {
                ++ this.values.begin.mins;
                ++ this.values.end.mins;

                this.title = interval_check.call(
                    this,
                    this.mess.mins.min_duration,
                    this.getDiff() <= this.min_diff.mins,
                    {border: 'end', unit: 'mins'}
                );

                overflow_recalc.call(this);
            }
        },
        end: {
            hours: function ()
            {
                this.values.end.hours ++;

                this.title = interval_check.call(
                    this,
                    this.mess.overflow.end,
                    !overflow_check.call(this.values),
                    {border: 'end', unit: 'hours'}
                );
            },
            mins: function () {
                this.values.end.mins += this.min_diff.mins;
                overflow_recalc.call(this);

                this.title = interval_check.call(
                    this,
                    this.mess.overflow.end,
                    !overflow_check.call(this.values),
                    {border: 'end', unit: 'mins'}
                );
            }
        }
    },
    decrement: {
        begin: {
            hours: function ()
            {
                -- this.values.begin.hours;

                this.title = interval_check.call(
                    this,
                    this.mess.overflow.begin,
                    !overflow_check.call(this.values),
                    {border: 'begin', unit: 'hours'}
                );
            },
            mins: function () {
                -- this.values.begin.mins;
                -- this.values.end.mins;

                overflow_recalc.call(this);

                this.title = interval_check.call(
                    this,
                    this.mess.overflow.begin,
                    !overflow_check.call(this.values),
                    {border: 'begin', unit: 'hours'}
                );
            }
        },
        end: {
            hours: function () {
                this.values.begin.hours -= !Math.floor((this.getDiff() - 1) / this.min_diff.hours);
                --this.values.end.hours;

                this.title = interval_check.call(
                    this,
                    this.mess.hours,
                    this.getDiff() == this.min_diff.hours,
                    {border: 'begin', unit: 'hours'}
                );

                this.title = interval_check.call(
                    this,
                    this.mess.overflow.begin,
                    !overflow_check.call(this.values),
                    {border: 'begin', unit: 'hours'}
                );
            },
            mins: function () {
                this.values.end.mins -= (this.getDiff() == this.min_diff.mins) || this.min_diff.mins;
                this.values.begin.mins -= (this.getDiff() < this.min_diff.mins);

                this.title = interval_check.call(
                    this,
                    this.mess.mins.min_duration,
                    this.getDiff() <= this.min_diff.mins,
                    {border: 'begin', unit: 'mins'}
                );

                overflow_recalc.call(this);
            }
        }
    }
};

TimePicker.prototype.render = function (struct, parent) {

    // this.popover_element.classList.add('rendered');

    struct.forEach(function (element) {

        current_element = element.tag == 'TextNode'
            ? document.createTextNode(element.text)
            : document.createElement(element.tag);

        for(key in element.attr)
            current_element.setAttribute(key, element.attr[key]);

        parent.appendChild(current_element);

        // if(element.children !== undefined)
        //     this.render(element.children, current_element);
        !element.children || this.render(element.children, current_element);

    }, this);
};

TimePicker.prototype.init = function (event) {

    var hours_diff = Math.floor(event.offsetY / 30),
        mins_diff = (event.offsetY - hours_diff * 30) * 2;

    var values = {
        begin: {
            hours: hours_diff + 10,
            mins: mins_diff
        },
        end: {
            hours: Math.floor((mins_diff + 15) / 60) + hours_diff + 10,
            mins: (mins_diff + 15) % 60
        }
    };

    this.state = new State({values: values, getDiff: function () {return 15}});

    this.input.fill();

    return this;
};

TimePicker.prototype.addListeners = function (options)
{

    this.popover_element.onwheel = wheelMouse.bind(this);

    this.popover_element.onclick = clickArrow.bind(this);

    time_picker_success.onclick = clickSuccess.bind(this, options);

    time_picker_cancel.onclick = clickCancel;

    return this;
};

TimePicker.prototype.getTimeStamp = function (current_day_begin) {
    return {
        begin: to_unix_standard( new Date(current_day_begin * 1000).setHours(
                this.state.values.begin.hours,
                this.state.values.begin.mins,
                0,
                0
            )),
        end: to_unix_standard( new Date(current_day_begin * 1000).setHours(
                this.state.values.end.hours,
                this.state.values.end.mins,
                0,
                0
        ))
    }
};

TimePicker.prototype.hide = function(){
    console.log('destroy!!');
    $('.station-body').popover('destroy');
};

function clickArrow(event) {
    event.preventDefault();
    return event.target.tagName == 'A' ? this.setValue(event.target) : false;
}

function wheelMouse(event)
{
    event.preventDefault();

    var scroll_interval = event.timeStamp - prev_ts,
        delta = event.deltaY || event.detail || event.wheelDelta;

    prev_ts = event.timeStamp;

    return scroll_interval > min_mouse_sensitive && event.target.tagName == 'INPUT'
        ? this.setValue(event.target, delta > 1 ? 'decrement' : 'increment')
        : false;
}

function clickSuccess(options)
{
    var values = this.getTimeStamp(options.send.data.sts);

    $.ajax({
        method: 'post',
        url: options.send.url,
        data: {
            current: options.send.data.bocdts,
            selected: options.send.data.sts,
            values: values,
            station_name: this.station_name
        }
    })
        .done(options.send.done)

    this.hide();
}

function clickCancel(TP) {
    // $(this).parents('')
    console.log(this);
    // TP.hide();
}

function getBorderNodeList (border_name)
{
    var nodesListObj = {};

    ['hours', 'mins'].forEach(function (input_wrap_class) {

        nodesListObj[input_wrap_class] = this
            .getElementsByClassName(border_name)[0]
            .getElementsByClassName(input_wrap_class)[0]
            .getElementsByClassName('time-input')[0];


    }, this);


    return nodesListObj;
}

function overflow_recalc() {
    ['begin', 'end'].forEach(function (border) {

        this.values[border] = {
            hours: this.values[border].hours + Math.floor(this.values[border].mins / 60),
            mins: (this.values[border].mins % 60 + 60) % 60
        };

    }, this);
}

function overflow_check()
{
    return this.begin.hours >= 10 && (this.end.hours < 22 || (this.end.hours == 22 && this.end.mins == 0))
}

function interval_check(mess, condition, target) {
    var tmp_obj = {begin: {}, end: {}};
    tmp_obj[target.border][target.unit] = condition ? mess : false;

    return tmp_obj;
}

function getTermination(value)
{
    var value_termination = value % 10,
        word_termination = '';

    switch (value_termination) {
        case 1:
            word_termination = 'a';
            break;
        case 2:
        case 3:
        case 4:
            word_termination = 'и';
            break;
    }

    word_termination = value <= 10
        ? word_termination
        : '';

    return value ? value + ' годин' + word_termination + '<br>' : '';
}

HTMLElement.prototype.setCustomTooltip = function (title)
{
    var mess = title
        ? title[this.dataset.parent]
            ? title[this.dataset.parent][this.dataset.inner]
            : false
        : false;
    $(this).tooltip('destroy');
    !mess || $(this).tooltip({title: mess}).trigger('mouseenter');

    return this;
};