var Session = function (element) {
    this.element = element;
    this.begin = element.dataset.begin;
    this.end = element.dataset.end;

    var hour_begin = parseInt(this.begin.substr(0, 2));
    var min_begin = parseInt(this.begin.substr(3, 2));

    var mins_diff = parseInt(this.end.substr(3, 2)) - min_begin;
    var mins = mins_diff >= 0 ? mins_diff : 60 + mins_diff;

    var hours_diff = parseInt(this.end.substr(0, 2)) - hour_begin;
    var hours = mins_diff >= 0 ? hours_diff : --hours_diff;

    this.setCssProp(hour_begin, min_begin, hours, mins);

    this.render();
};

Session.prototype.render = function () {
    this.element.style.top = this.top + 'px';
    this.element.style.height = this.height + 'px';
    $(this.element).tooltip(options.tooltip(this));
};

Session.prototype.setCssProp = function (hour_begin, min_begin, hours_interval, mins_interval) {
    this.top = (hour_begin - 10) * 30 + min_begin / 2;
    this.height = 30 * hours_interval + mins_interval / 2;
};