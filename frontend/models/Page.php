<?php

namespace frontend\models;


/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $sort
 * @property string $meta_title
 * @property string $meta_desc
 * @property string $meta_key
 * @property string $url
 * @property string $h1
 * @property string $content
 *
 * @property PageType $type
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort', 'meta_title', 'meta_desc', 'meta_key', 'url', 'h1', 'content'], 'required'],
            [['sort'], 'integer'],
            [['content'], 'string'],
            [['meta_title', 'meta_desc', 'meta_key', 'url', 'h1'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sort' => 'Sort',
            'meta_title' => 'Meta Title',
            'meta_desc' => 'Meta Desc',
            'meta_key' => 'Meta Key',
            'url' => 'Url',
            'h1' => 'H1',
            'content' => 'Content',
        ];
    }
}
