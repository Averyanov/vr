<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $tel;
    public $body;
    public $verifyCode;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'body'], 'required', 'message' => 'Це поле не може бути порожнім'],
            ['email', 'email'],
            ['verifyCode', 'captcha'],
            ['tel', 'required', 'when' => function ($model) {
                return $model->email == '';
            }, 'whenClient' => "function (attribute, value) {
                    return $('#contactform-email').val() == '';
                }", 'message' => 'Залишіть, хоча б, електронну адресу'
            ],
            ['email', 'required', 'when' => function ($model) {
                return $model->tel == '';
            }, 'whenClient' => "function (attribute, value) {
                    return $('#contactform-tel').val() == '';
                }", 'message' => 'Залишіть, хоча б, номер телефону'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom(['me@me.com' => $this->name])
            ->setSubject('Mail from '.$this->name)
            ->setTextBody($this->body.$this->email.$this->tel)
            ->send();
    }
}
