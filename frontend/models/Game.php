<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "game".
 *
 * @property integer $id
 * @property string $name
 * @property string $genre
 * @property string $release_date
 * @property string $multiplayer
 * @property string $developer
 * @property string $img
 * @property string $description
 */
class Game extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
//        return [
//            [['name', 'genre', 'release_date', 'multiplayer', 'developer', 'img', 'description'], 'required'],
//            [['multiplayer', 'description'], 'string'],
//            [['name', 'genre', 'release_date', 'developer', 'img'], 'string', 'max' => 255],
//        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'genre' => 'Genre',
            'release_date' => 'Release Date',
            'multiplayer' => 'Multiplayer',
            'developer' => 'Developer',
            'img' => 'Img',
            'description' => 'Description',
        ];
    }
}
