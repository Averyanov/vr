<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "page_type".
 *
 * @property integer $id
 * @property string $type
 * @property string $text
 */
class PageType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'text'], 'required'],
            [['type'], 'string', 'max' => 63],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'text' => 'Text',
        ];
    }
}
