<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "newsletter".
 *
 * @property integer $id
 * @property string $email
 */
class Newsletter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newsletter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required', 'message' => 'Поле e-mail не може бути порожнім'],
            [['email'], 'email', 'message' => 'Вказана Вами адреса невалідна'],
            [['email'], 'unique', 'message' => 'Вказана Вами адреса вже зареєстрована'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => 'cusotm user'])
            ->setSubject('Newsletter')
            ->setTextBody($this->email.' want to read our news')
            ->send();
    }
}
