<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class PhotoAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/least/least.min.css',
        'css/least/least.custom.css',
        'css/least/least.nav.css',
    ];
    public $js = [
        'js/least/least.min.js',
        'js/least/least.nav.js',
        'js/jquery.lazyload.js'
    ];
    public $depends = [
        'frontend\assets\InnerAppAsset',
    ];
}
