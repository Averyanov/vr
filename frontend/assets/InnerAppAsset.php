<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class InnerAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Open+Sans:400,600,700',
        'css/bootstrap.css',
        'css/responsive.css',
        'css/style.css',
    ];
    public $js = [
        'js/toTop.js',
        'js/device.min.js',
        'js/bootstrap.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
