<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class DatepickerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/datepicker/bootstrap-datepicker.min.css',
        'css/datepicker/custom.css',
        'css/timepicker/style.css'
    ];
    public $js = [
        'js/datepicker/bootstrap-datepicker.min.js',
        'js/datepicker/locales/bootstrap-datepicker.uk.min.js',
        'js/datepicker/time_picker.struct.js',
        'js/datepicker/custom.js',
        'js/classes/session.class.js',
        'js/classes/timepicker.class.js'
    ];
    public $depends = [
        'frontend\assets\InnerAppAsset',
    ];
}
