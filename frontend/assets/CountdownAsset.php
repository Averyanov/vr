<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class CountdownAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/countdown/jquery.countdown.css'
    ];
    public $js = [
        'js/countdown/script.js',
        'js/countdown/jquery.countdown.js',
    ];
    public $depends = [
        'frontend\assets\InnerAppAsset',
    ];
}
