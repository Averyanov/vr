<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class NewsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/news/news.css'
    ];
    public $js = [
    ];
    public $depends = [
        'frontend\assets\InnerAppAsset',
    ];
}
