<?
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\web\View;

use frontend\controllers\SiteController;
use frontend\assets\IndexAsset;

IndexAsset::register($this);

$this->registerJs(
    '
        $(document).ready(function(){
            $(".main-game-list-item .caption").on("click", function()
            {
                $(this)
                    .siblings(".thumbnail-pad")
                    .find(".game-link")[0]
                    .click();
            })
        });
    ',
    View::POS_END);

$this->title = 'Головна';

echo ListView::widget([
    'dataProvider' => Yii::$app->controller->indexDataProvider,
    'layout' => '{items}',
    'options' => [
        'tag' => 'div',
        'class' => 'row main-view'
    ],
    'itemView' => function($model)
    {
        return $model->id == Yii::$app->controller->getId('game')
            ? ListView::widget([
                'dataProvider' => Yii::$app->controller->mainGameDataProvider,
                'layout' => '{items}',
                'options' => [
                    'tag' => 'ul',
                    'class' => 'thumbnails',
                ],
                'itemView' => '/_game_item',
                'itemOptions' => [
                    'tag' => 'li',
                    'class' => 'span3'
                ]
            ])
            : $model->content;
    },
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'span12'
    ]
]);
