<?
use frontend\assets\DatepickerAsset;

DatepickerAsset::register($this);
?>
<section id="content" class="main-content">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div id="sandbox-container">
                    <div></div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="modal" class="modal fade booking-popup" tabindex='-1'>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Оберіть час та ігрове місце</h3>
    </div>
    <div id="modal_body" class="modal-body">
    </div>
    <div class="modal-footer">
<!--        <a href="#" class="btn btn-primary reserve">Зарезервувати</a>-->
    </div>
</div>