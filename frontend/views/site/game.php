<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use yii\widgets\ListView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = Html::a('Ігри', Url::to('/games'));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="span9">
        <h4><?= $model->name ?></h4>
        <div class="text-block-1">
            <?= Html::img([$model->img], ['class' => 'img-radius'])?>
            <div>
                <p class="lead p2 game-desc"><?= $model->description ?></p>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="game-prop">
            <div class="genre"> <span class="caption">Жанр: </span> <?= $model->genre ?></div>
            <div class="release_date"><span class="caption">Дата релізу: </span><?= $model->release_date ?></div>
            <div class="multiplayer"><span class="caption">Мультиплееєр: </span><?= $model->multiplayer == 'yes' ? 'Багатокористувацька' : 'Однокористувацька' ?></div>
            <div class="developer"><span class="caption">Розробники: </span><?= $model->developer ?></div>
        </div>
        <div class="video-wrap">
            <?= $model->video ?>
        </div>
    </div>
    <div class="span3">
        <h4>Також</h4>
        <?=
        ListView::widget([
            'dataProvider' => Yii::$app->controller->gameDataProvider,
            'layout' => '{items}',
            'options' => [
                'tag' => 'ul',
                'class' => 'list'
            ],
            'itemView' => function($model)
            {
                $items = Html::tag('div', $model->genre).
                    Html::tag('div',$model->release_date, ['class' => 'release_date']).
                    Html::tag('div', $model->multiplayer == 'yes' ? 'Багатокористувацька' : 'Однокористувацька').
                    Html::tag('div',$model->developer, ['class' => 'developer']);

                $hidden_prop = Html::tag('div',$items, ['class' => 'hidden-prop']);
                return Html::a($model->name, '/games/'.$model->id).$hidden_prop;
            },
            'itemOptions' => [
                'tag' => 'li'
            ]
        ])
        ?>
    </div>
</div>