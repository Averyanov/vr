<?
use yii\widgets\ListView;
use yii\helpers\Html;
?>
<?=
Html::beginForm(
    ['site/search'],
    'post',
    [
        'data-pjax' => '',
        'class' => 'navbar-form',
        'id' => 'search_form'
    ]) ?>

    <?=
    Html::input(
        'text',
        'search-string',
        Yii::$app->request->post('search-string'),
        [
            'autocomplete' => 'off',
            'id' => 'search_input'
        ]) ?>

    <?=
    Html::input(
        'submit',
        'search-submit',
        '',
        ['class' => 'search']);
    ?>
    <? if(!isset($noresult))
    {
        echo ListView::widget([
            'dataProvider' => $resultDataProvider,
            'emptyText' => Html::tag(
                'span',
                'Не знайдено жодного співпадіння',
                ['class' => 'find-no-reslut']
            ),
            'layout' => '{items}',
            'options' => [
                'tag' => 'div',
                'class' => 'search-result'
            ],
            'itemView' => function($model)
            {
                return Html::a($model->name, '/games/'.$model->id, ['data-pjax' => 0 ]);
            },
            'itemOptions' => [
                'tag' => 'div'
            ]
        ]);
    }
    ?>
<?= Html::endForm() ?>