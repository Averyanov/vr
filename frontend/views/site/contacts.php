<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
$this->title = $title;
$this->params['breadcrumbs'][] = $this->title ;
?>
<div class="row">
    <div class="span4">
        <h4>Контактна інформація</h4>
        <div class="map">
            <?=
            Html::tag(
                'iframe',
                false,
                [
                    'src' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2551.1560192374536!2d28.655511115724185!3d50.251670279448454!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x472c6491f4ebf907%3A0x22fb5b1ac6eda24a!2z0LLRg9C70LjRhtGPINCn0LXRgNC90Y_RhdC-0LLRgdGM0LrQvtCz0L4sIDExLCDQltC40YLQvtC80LjRgCwg0JbQuNGC0L7QvNC40YDRgdGM0LrQsCDQvtCx0LvQsNGB0YLRjA!5e0!3m2!1suk!2sua!4v1478712477959'
                ]
            )
            ?>
        </div>
        <address>
            <?=
            Html::tag(
                'strong',
                Yii::$app->controller->contacts['address'],
                ['class' => 'clr']
            )
            ?>
            <br>
            <span>Телефон: </span>
            <?= Yii::$app->controller->contacts['tel'] ?>
            <br>
            E-mail:
            <?=
            Html::tag('u', Yii::$app->formatter->asEmail(Yii::$app->controller->contacts['e-mail']))
            ?>
        </address>

    </div>
    <div class="span6">
        <h4>Будемо на зв'язку</h4>
        <div class="contact-form">
            <? $form = ActiveForm::begin(['id' => 'contact-form']); ?>
            <fieldset>
                <?=
                    $form->field($model, 'name', [
                        'inputTemplate' => '<label class="name">{input}<span class="error">error</span><span class="empty">empty</span></label>',
                    ])
                        ->textInput(['placeholder' => 'Ваше ім`я'])
                        ->label(false);
                ?>

                <?=
                $form->field($model, 'email', [
                    'inputTemplate' => '<label class="email">{input}<span class="error">error</span><span class="empty">empty</span></label>',
                ])
                    ->textInput(['placeholder' => 'Електронна адреса'])
                    ->label(false);
                ?>

                <?=
                $form->field($model, 'tel', [
                    'inputTemplate' => '<label class="tel">{input}<span class="error">error</span><span class="empty">empty</span></label>',
                ])
                    ->textInput(['placeholder' => 'Номер телефону'])
                    ->label(false);
                ?>

                <?=
                $form->field($model, 'body', [
                    'inputTemplate' => '<label class="body">{input}<span class="error">error</span><span class="empty">empty</span></label>',
                ])
                    ->textarea(['placeholder' => 'Тест повідомлення'])
                    ->label(false);
                ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<label class="verifyCode">{image}</label>{input}',
                ])->label(false) ?>
            </fieldset>
            <div class="pull-right">
                <?= Html::resetButton('Очистити форму', ['class' => 'btn btn_ btn-small_', 'name' => 'contact-button']) ?>
                <?= Html::submitButton('Відправити', ['class' => 'btn btn_ btn-small_', 'name' => 'contact-button']) ?>
            </div>

            <? ActiveForm::end(); ?>
        </div>
    </div>
</div>
