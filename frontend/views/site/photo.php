<?
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\web\View;
use frontend\assets\PhotoAsset;

PhotoAsset::register($this);

$this->registerJs('$(document).ready(function(){$("#gallery").least();});', View::POS_END);
$this->title = $model->meta_title;
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="content" class="main-content">
    <div class="container">
        <div class="row">
            <div class="span12">
                <!--  least.js gallery -->
                <section class="gallery">
                    <ul id="gallery">
                        <li id="fullPreview"></li>
                        <?=
                        ListView::widget([
                            'dataProvider' => $photoDataProvider,
                            'layout' => '{items}',
                            'options' => [
                                'tag' => false
                            ],
                            'itemView' => function($model, $key, $index)
                            {
                                $result = Html::a('',$model->img);
                                $result .= Html::img(
                                    'img/effects/white.gif',
                                    [
                                        'data-original' => $model->thumb,
                                        'alt' => 'Фото '.($index+1)
                                    ]
                                    );
                                $result .= Html::tag('div','',['class' => 'overLayer']);
                                $info_layer_list = [
                                    Html::tag('h2', 'Фото '.($index+1)),
                                    Html::tag('p', 'Переглянути')
                                ];
                                $info_layer_content = Html::ul($info_layer_list);
                                $result .= Html::tag('div', $info_layer_content, ['class' => 'infoLayer']);
                                $result .= Html::tag('div', $model->description, ['class' => 'projectInfo']);

                                return Html::decode($result);
                            },
                            'itemOptions' => [
                                'tag' => 'li',
                                'class' => false,
                                'id' => false
                            ]
                        ])
                        ?>
                    </ul>
                    <div class="gallery-nav" id="galleryNav">
                        <nav class="nav-prev fa fa-arrow-left" data-direct="-2"></nav>
                        <nav class="nav-next fa fa-arrow-right" data-direct="0"></nav>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
