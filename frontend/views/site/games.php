<?

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;

use yii\web\View;

$this->title = $model->meta_title;
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
    '
        $(document).ready(function(){
            $(".game-list-item img").on("click", function()
            {
                $(this)
                    .parent("figure")
                    .siblings(".more-inf")[0]
                    .click();
            })
        });
    ',
    View::POS_END);

?>
<div class="row">
    <div class="span9">
        <div class="thumbnails_3">
            <?=
            ListView::widget([
                'dataProvider' => Yii::$app->controller->gameDataProvider,
                'layout' => '{items}',
                'options' => [
                    'tag' => 'div',
                    'class' => 'thumbnail thumbnail_3 game-list-wrap'
                ],
                'itemView' => function($game)
                {
                    return Html::tag(
                        'figure',
                        Html::img([$game->img], ['class' => 'img-radius'])
                    ).
                    Html::tag('p',$game->name, ['class' => 'lead p2']).
                    $game->description.
                    '<br>'.
                    Html::a('детальніше', '/games/'.$game->id, ['class' => 'btn btn_ more-inf']);
                },
                'itemOptions' => [
                    'tag' => 'div',
                    'class' => 'span4 game-list-item'
                ]
            ])
            ?>
    </div>
        <h5 class="annotation">Якщо Ви не знайшли очукувану гру, то <a href="/contacts">запропонуйте її</a></h5>
    </div>
    <div class="span3">
        <h4 class="indent-2">Останні новини:</h4>
        <?=
        ListView::widget([
            'dataProvider' => $newsDataProvider,
            'layout' => '{items}',
            'options' => [
                'tag' => 'ul',
                'class' => 'list-news'
            ],
            'itemView' => function($model)
            {
                $date_link = Html::a(Yii::$app->formatter->asDate($model->date), '/news/'. $model->id, ['class' => 'btn btn_']);
                $header = Html::tag('p', $model->header, ['class' => 'text-info']);
                $link = ' '.Html::a('>>' ,'/news/'. $model->id, ['class' => 'underline']);

                return $date_link.$header.$model->short_description.$link;
            },
            'itemOptions' => [
                'tag' => 'li'
            ]
        ])
        ?>
    </div>
</div>