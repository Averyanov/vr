<?
use yii\widgets\ListView;
?>

<?
echo ListView::widget([
    'dataProvider' => $reservedDataProvider,
    'layout' => '{items}',
    'itemView' => 'station/day',
    'itemOptions' => [
        'class' => 'station'
    ]
])
?>