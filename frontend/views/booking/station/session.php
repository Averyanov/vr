<?
use yii\helpers\Html;

$begin = substr($model['begin'], -8, 5);
$end = substr($model['end'], -8, 5);
?>

<?=
Html::tag(
    'div',
    null,
    [
        'class' => 'station-session',
        'data-begin' => $begin,
        'data-end' => $end,
    ]
)
?>