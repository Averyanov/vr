<?
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\data\ArrayDataProvider;

$name = array_shift($model);
$sessionDataProvider = new ArrayDataProvider([
    'allModels' => $model
]);
?>

<?=
Html::tag('span', $name, ['class' => 'staion-caption'])
?>

<?=
ListView::widget([
    'dataProvider' => $sessionDataProvider,
    'layout' => '{items}',
    'itemView' => 'session',
    'itemOptions' => [
        'tag' => null
    ],
    'options' => [
        'class' => 'station-body'
    ],
    'emptyText' => false
])
?>
