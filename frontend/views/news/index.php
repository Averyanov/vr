<?
use yii\widgets\ListView;
use frontend\assets\NewsAsset;

NewsAsset::register($this);

$this->title = $page_model->meta_title;
$this->params['breadcrumbs'][] = $this->title ;
?>

<?=
ListView::widget([
    'dataProvider' => $newsDataProvider,
    'layout' => '{items}',
    'options' => [
        'tag' => false,
    ],
    'itemView' => '_list_item',
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'row'
    ]
])
?>

