<?
use yii\helpers\Html;

$item_html =
    Html::tag('h4',
        Html::a($model->header, '/news/'.$model->id)
    ).
    Html::tag('p', $model->short_description, ['class' => 'description']).
    Html::tag('p', $model->date);
?>
<?=
Html::tag('div', $item_html,['class' => 'span12'])
?>

