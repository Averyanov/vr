<?
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\NewsAsset;

NewsAsset::register($this);

$this->title = $model->header;
$this->params['breadcrumbs'][] = Html::a('Новини', Url::to('/news'));
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div class="span12">
        <?=
        Html::tag('h4', $model->header)
        ?>
    </div>
</div>
<div class="row">
    <div class="span12">
        <?=
        Html::tag('p', $model->text, ['class' => 'text'])
        ?>
    </div>
</div>
