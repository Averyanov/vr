<?
use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\widgets\ListView;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head();?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="container" id="container">
    <div class="positioned outer-item top end" params-name="end_window" ></div>
    <!--header of frame-->

    <!--top corners of frame-->
    <div class="positioned outer-item corner top left" params-name="outer_corner_top" ></div>
    <div class="positioned outer-item corner top right" params-name="outer_corner_top" ></div>
    <!--top corners of frame-->

    <!--sides of frame-->
    <div class="positioned outer-item left side" params-name="side" ></div>
    <div class="positioned outer-item right side" params-name="side" ></div>
    <!--sides of frame-->

    <!--bottom corners of frame-->
    <div class="positioned outer-item corner bottom left" params-name="outer_corner_bottom" ></div>
    <div class="positioned outer-item corner bottom right" params-name="outer_corner_bottom" ></div>
    <!--bottom corners of frame-->

    <!--bottom of frame-->
    <div class="positioned outer-item bottom end" params-name="end_window" ></div>
    <!--bottom of frame-->


    <div class="positioned inner-wrap" id="inner_wrap" params-name="inner_wrap">
        <!--top corners-->
        <div class="positioned inner-item top left corner" params-name="inner_corner_top"></div>
        <div class="positioned inner-item top right corner" params-name="inner_corner_top"></div>
        <!--top corners-->

        <!--chasers-->
        <div class="positioned inner-item top chaser" params-name="chaser"></div>
        <div class="positioned inner-item bottom chaser" params-name="chaser"></div>
        <!--chasers-->

        <!--bottom corners-->
        <div class="positioned inner-item bottom left corner" params-name="inner_corner_bottom"></div>
        <div class="positioned inner-item bottom right corner" params-name="inner_corner_bottom"></div>
        <!--bottom corners-->

        <!--conent-->
        <?= $content ?>
        <!--conent-->
    </div>
</div>

<!--menu-->
<div class="menu" id="menu">
<?=
ListView::widget([
    'dataProvider' => Yii::$app->controller->menuDataProvider,
    'layout' => '{items}',
    'options' => ['tag' => 'ul', 'id' => false],
    'itemView' => '/_menu_item',
    'itemOptions' => ['tag' => 'li']
])
?>
</div>
<!--menu-->

<!--logo-->
<div class="logo" id="logo"></div>
<!--logo-->

<!--photo box shadow-->
<div class="photo-shadow" id="photo_shadow"></div>
<!--photo box shadow-->

<!--photo box-->
<div class="photo-box disable" id="photo_box">
    <div class="top border"></div>
    <div class="bottom border"></div>
    <div class="nav left" data-direction="-1"></div>
    <div class="nav right" data-direction="1"></div>
</div>
<!--photo box-->
<? $this->endBody() ?>
</body>
</html>
<? $this->endPage() ?>
