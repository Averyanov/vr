<?
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;

use frontend\widgets\Carousel;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use frontend\controllers\SiteController;

use frontend\assets\InnerAppAsset;
use frontend\assets\CountdownAsset;



InnerAppAsset::register($this);

$this->registerJs(
    '$("#search_form_wrap").on("keyup", "input", function()
    {
        $("#search_form").submit();
    });
    
    $(document).on("pjax:complete", function()
    {
        var end_position = $("#search_input")[0].value.length;
        $("#search_input").focus();
        $("#search_input")[0].setSelectionRange(end_position, end_position);
        
    });
    '
);

$this->beginPage();

Url::remember();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon"
          type="image/x-icon"
          href="/favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <? $this->head() ?>
</head>
<body>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-85190971-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
<? $this->beginBody() ?>
<!--==============================header=================================-->
<header class="p0">
    <div class="container">
        <div class="row">
            <div class="span12">
                <?
                if(Yii::$app->controller->countdown)
                {
                    CountdownAsset::register($this);
                    $h4_html = 'до кінця '.Html::a('акції','/price').' лишилось';

                    echo
                        Html::tag('h4', $h4_html, ['class' => 'action']).
                        Html::tag('div', null, ['id' => 'countdown']);
                }
                else
                {
                    $h4_html = 'увага '.Html::a('акція','/price');
                    echo Html::tag('h4', $h4_html, ['class' => 'action']);
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="span12">
                <div class="header-block clearfix">
                    <div class="clearfix header-block-pad">
                        <h1 class="brand"><a href="/"><img src="/img/logo_inner.png" alt=""></a></h1>
                        <? Pjax::begin([
                            'enablePushState' => false,
                            'options' => [
                                'class' => 'search-form-wrap',
                                'id' => 'search_form_wrap'
                            ]
                        ]) ?>
                        <?= Html::beginForm(
                            ['site/search'],
                            'post',
                            [
                                'data-pjax' => '',
                                'class' => 'navbar-form',
                                'id' => 'search_form'
                            ]) ?>
                            <?= Html::input(
                                'text',
                                'search-string',
                                null,
                                [
                                    'autocomplete' => 'off',
                                    'id' => 'search_input'
                                ]) ?>
                            <?= Html::input('submit', 'search-submit', '', ['class' => 'search']);?>
                        <?= Html::endForm() ?>
                        <? Pjax::end()?>
                        <span class="contacts">Телефон:
                            <span><?= Yii::$app->controller->contacts['tel'] ?></span>
                            <br>E-mail: <?= Yii::$app->formatter->asEmail(Yii::$app->controller->contacts['e-mail']) ?>
                        </span>
                    </div>
                    <div class="navbar navbar_ clearfix">
                        <div class="navbar-inner navbar-inner_">
                            <div class="container">
                                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse_">MENU</a>
                                <div class="nav-collapse nav-collapse_ collapse">
                                <?=
                                    ListView::widget([
                                        'dataProvider' => Yii::$app->controller->menuDataProvider,
                                        'layout' => '{items}',
                                        'options' => [
                                            'tag' => 'ul',
                                            'id' => false,
                                            'class' => ['nav', 'sf-menu']
                                        ],
                                        'itemView' => function($model, $key, $index)
                                        {
                                            return $model->id == Yii::$app->controller->getId('game')
                                                ? Html::tag(
                                                    'li',
                                                    Html::a($model->h1, '/'.$model->url).
                                                    ListView::widget([
                                                        'dataProvider' => Yii::$app->controller->gameDataProvider,
                                                        'layout' => '{items}',
                                                        'options' => [
                                                            'tag' => 'ul',
                                                            'id' => false,
                                                        ],
                                                        'itemView' => function($game)
                                                        {
                                                            return Html::a($game->name, '/games/'.$game->id);
                                                        },
                                                        'itemOptions' => [
                                                            'tag' => 'li'
                                                        ]
                                                    ]),
                                                    [
                                                        'class' => 'sub-menu'
                                                    ]
                                                )
                                                : Html::tag(
                                                    'li',
                                                    Html::a(
                                                        isset($model->h1) ? $model->h1 : '<em class="hidden-phone"></em><span class="visible-phone">Головна</span>',
                                                        '/'.$model->url
                                                    ),
                                                    ['class' => $index == 0 ? 'li-first' : false]
                                                );
                                        },
                                        'itemOptions' => ['tag' => false]
                                    ])
                                ?>
                                </div>
                                <ul class="social-icons">
                                    <li><a href="https://vk.com/vrarenaclub" target="_blank"><img src="/img/vk.png" alt=""></a></li>
                                    <li><a href="https://facebook.com/vrarenaclub" target="_blank"><img src="/img/fb.png" alt=""></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?/*=
    Html::tag(
        'video',
        Html::tag('source',null,['src' => '/upload/video/main/htc_vive_intro.mp4', 'type' => 'video/mp4']),
        ['playsinline' => true, 'autoplay' => true, 'muted' => true, 'loop' => true, 'class' => 'main-bgd']
    );
    */?>
    <div class="container">
        <div class="row">
            <div class="span12">
                <?= Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => '<i class = "fa fa-home fa-2x"></i>',
                        'url' => Yii::$app->homeUrl,
                        'template' => '<li>{link}</li>',
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'encodeLabels' => false,
                ]) ?>
            </div>
        </div>
    </div>
    <? if(Yii::$app->controller->camera) {?>
        <div class="container">
            <div class="row">
                <div class="span12 carousel" id="carousel">
                    <?=
                    Carousel::widget([
                        'items' => Yii::$app->controller->photoDataProvider,
                    ]);
                    ?>
                </div>
            </div>
        </div>
    <?}?>
</header>
<section id="content" class="content">
    <div class="container">
        <?if(Yii::$app->session->getFlash('success') || Yii::$app->session->getFlash('error')){?>
        <div class="row">
            <div class="success span12">
                <strong>
                    <?= Yii::$app->session->getFlash('success'); ?>
                </strong>
            </div>
            <div class="error span12">
                <strong>
                    <?= Yii::$app->session->getFlash('error'); ?>
                </strong>
            </div>
        </div>
        <?}?>
        <?= $content ?>
    </div>
</section>

<div class="both"></div>
<footer>
    <div class="container">
        <div class="row">
            <div class="span4 float2">
                <? $form = ActiveForm::begin([
                    'id' => 'newsletter',
                    'action' => '/site/subscribe',
                    'enableClientValidation' => true,
                    'enableAjaxValidation' => true,
                    'options' => ['class' => 'newsletter']
                ]); ?>
                <div class="clearfix">
                    <?=
                    $form->field(
                        Yii::$app->controller->newsletter,
                        'email'
                    )->textInput([
                        'placeholder' => 'Електронна адреса',
                        'class' => 'newsletter-email'
                    ])->label(false)
                    ?>
                    <?= Html::submitButton('Підписатись', ['class' => 'btn btn_', 'name' => 'contact-button']) ?>
                </div>
                <? ActiveForm::end(); ?>
            </div>
            <div class="span8 float">
                <?=
                Html::ul(
                    ArrayHelper::map(Yii::$app->controller->pages, 'id', function($element)
                    {
                        return [
                            'url' => $element->url,
                            'caption' => $element->h1
                        ];
                    }),
                    [
                        'class' => 'footer-menu',
                        'item' => function($item)
                        {
                            $link = Html::a($item['caption'], '/'.$item['url']);
                            return $itemTag = Html::tag('li', $link);
                        },
                    ]
                )
                ?>
            </div>
        </div>
    </div>
</footer>
<? $this->endBody() ?>
<a href="#" id="toTop"><span id="toTopHover"></span></a>
</body>
</html>
<? $this->endPage() ?>