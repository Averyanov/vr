<div class="thumbnail main-game-list-item">
    <div class="caption">
      	<img src="<?= $model->img ?>" alt="">
        <br>
        <h3><?= $model->name ?></h3>
    </div>
    <div class="thumbnail-pad">
        <p><?= $model->description ?></p>
        <a href="/games/<?= $model->id ?>" class="btn btn_ game-link">більше</a>
    </div>
</div>