<?
use yii\widgets\ListView;
?>

<?=
ListView::widget([
    'dataProvider' => $gameDataProvider,
    'layout' => '{items}',
    'options' => [
        'tag' => false
    ],
    'itemView' => '_game_item',
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'game-item'
    ]
])
?>
