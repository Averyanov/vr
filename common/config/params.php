<?php
return [
    'adminEmail' => 'trdkresalo@ukr.net',
    'supportEmail' => 'trdkresalo@ukr.net',
    'user.passwordResetTokenExpire' => 3600,
];
