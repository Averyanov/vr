<?php

namespace common\models;

use DateTime;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "time_interval".
 *
 * @property integer $id
 * @property string $begin
 * @property string $end
 */
class TimeInterval extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const FORMAT = 'Y-m-d H:i:s';

    public static function tableName()
    {
        return 'time_interval';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['begin', 'end'], 'required'],
            [['begin', 'end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'begin' => 'Begin',
            'end' => 'End',
        ];
    }

    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['time_interval_id' => 'id']);
    }

    public function getIntervalsByStation($selected_timeStamp)
    {
        date_default_timezone_set('Europe/Kiev');

        $begin = self::getDatetime($selected_timeStamp);

        list($y,$m,$d) = explode('-', date('Y-m-d', $selected_timeStamp));
        $end_time_stamp = mktime(0,0,0,$m,$d+1,$y);

        $end = self::getDatetime($end_time_stamp - 1);

        $intervals = $this
            ->find()
            ->select([
                'time_interval.begin',
                'time_interval.end',
                StationSpecimen::NAME
            ])
            ->join('join', 'booking b', 'time_interval.id = b.time_interval_id')
            ->join('join', 'station_specimen', 'b.station_specimen_id = station_specimen.id')
            ->join('join', 'station_type', 'station_specimen.station_type_id = station_type.id')
            ->where(['>=', 'time_interval.begin', $begin])
            ->andWhere(['<=', 'time_interval.end', $end])
            ->asArray()
            ->all();

        $intervalsByStation = ArrayHelper::index($intervals, null, 'name');

        $station_model = new StationSpecimen();

        $station_array = $station_model->getStationNameArray();

        $intervalsByStation = ArrayHelper::merge($station_array, $intervalsByStation);

        return $intervalsByStation;
    }

    public function isAvaliable($values, $station_name)
    {

        $begin = self::getDatetime($values['begin']);
        $end = self::getDatetime($values['end']);

        return $this
                ->find()
                ->select([
                    'time_interval.begin',
                    'time_interval.end'
                ])
                ->join('join', 'booking b', 'time_interval.id = b.time_interval_id')
                ->join('join', 'station_specimen', 'b.station_specimen_id = station_specimen.id')
                ->join('join', 'station_type', 'station_specimen.station_type_id = station_type.id')
                ->where(['and', ['<=', 'time_interval.begin', $begin], ['>', 'time_interval.end', $begin]])
                ->orWhere(['and', ['<', 'time_interval.begin', $end], ['>=', 'time_interval.end', $end]])
                ->orWhere(['and', ['>=', 'time_interval.begin', $begin], ['<=', 'time_interval.end', $end]])
                ->andWhere(['concat(station_type.name, " ", station_specimen.id)' => $station_name])
                ->all()
            ? false
            : true;
    }

    public static function getDatetime($timestamp, $format = self::FORMAT)
    {
        $dateObj = new DateTime();

        return $dateObj->setTimestamp($timestamp)->format($format);
    }

    public function createInterval($timestamp_array)
    {
        $this->setAttributes([
            'begin' => self::getDatetime($timestamp_array['begin']),
            'end' => self::getDatetime($timestamp_array['end'])
        ]);

        $this->save();

        return $this;
    }
}
