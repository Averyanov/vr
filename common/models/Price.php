<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "price_list".
 *
 * @property integer $id
 * @property integer $discount_id
 * @property integer $mins
 * @property integer $price
 *
 * @property DiscountList $discount
 */
class Price extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['discount_id', 'mins', 'price'], 'required'],
            [['discount_id', 'mins', 'price'], 'integer'],
            [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discount::className(), 'targetAttribute' => ['discount_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discount_id' => 'Discount ID',
            'mins' => 'Mins',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscount()
    {
        return $this->hasOne(Discount::className(), ['id' => 'discount_id']);
    }

    public static function getList()
    {
        return ArrayHelper::map(
            self
                ::find()
                ->select([
                    'discount.name',
                    'price.mins',
                    'price.price'
                ])
                ->join('join', 'discount', 'price.discount_id = discount.id')
                ->asArray()
                ->all(),
            'mins',
            'price',
            'name'
        );
    }
}
