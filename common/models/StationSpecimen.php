<?php

namespace common\models;

/**
 * This is the model class for table "station_specimen".
 *
 * @property integer $id
 * @property integer $station_type_id
 *
 * @property Booking[] $bookings
 * @property StationType $stationType
 */
class StationSpecimen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const NAME = 'concat(station_type.name, " ", station_specimen.id) name';

    public static function tableName()
    {
        return 'station_specimen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['station_type_id'], 'required'],
            [['station_type_id'], 'integer'],
            [['station_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => StationType::className(), 'targetAttribute' => ['station_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'station_type_id' => 'Station Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStationType()
    {
        return $this->hasOne(StationType::className(), ['id' => 'station_type_id']);
    }

    public function getStationNameArray()
    {
        return $this
            ->find()
            ->select([self::NAME])
            ->join('join', 'station_type', 'station_specimen.station_type_id = station_type.id')
            ->asArray()
            ->indexBy('name')
            ->all();
    }

    public static function getIdByName($station_name)
    {
        return self::
            find()
            ->join('join', 'station_type', 'station_specimen.station_type_id = station_type.id')
            ->where(['concat(station_type.name, " ", station_specimen.id)' => $station_name])
            ->one()
            ->id;
    }
}
