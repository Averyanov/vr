<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DiscountList */

$this->title = 'Create Discount List';
$this->params['breadcrumbs'][] = ['label' => 'Discount Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
