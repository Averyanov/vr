<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <ul>
        <li><a href="/page">Сторінки</a></li>
        <li><a href="/game">Ігри</a></li>
        <li><a href="/photo">Фото</a></li>
        <li><a href="/contacts">Контакти</a></li>
        <li><a href="/news">Новини</a></li>
        <li><a href="/slider">Слайдер на головній</a></li>
        <li><a href="/newsletter">Підписники</a></li>
        <li>
            <span>Ігрові станції</span>
            <ul>
                <li><a href="/station-type">Типи гарнітур</a></li>
                <li><a href="/station-specimen">Ігрові станції</a></li>
            </ul>
        </li>
        <li>
            <span>Налаштування</span>
            <ul>
                <li><a href="/discount">Типи тарифів</a></li>
                <li><a href="/price">Ціни</a></li>
            </ul>
        </li>
    </ul>
</div>
