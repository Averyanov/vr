<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Discount;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\PriceList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="price-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form
        ->field($model, 'discount_id')
        ->dropDownList(ArrayHelper::map(Discount::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'mins')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
