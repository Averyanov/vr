<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;

use backend\models\StationType;

/* @var $this yii\web\View */
/* @var $model backend\models\StationSpecimen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="station-specimen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form
        ->field($model, 'station_type_id')
        ->dropDownList(ArrayHelper::map(StationType::find()->all(), 'id', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
