<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\StationSpecimen */

$this->title = 'Update Station Specimen: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Station Specimens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="station-specimen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
