<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\StationSpecimen */

$this->title = 'Create Station Specimen';
$this->params['breadcrumbs'][] = ['label' => 'Station Specimens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="station-specimen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
