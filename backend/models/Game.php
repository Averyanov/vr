<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "game".
 *
 * @property integer $id
 * @property string $name
 * @property string $genre
 * @property string $release_date
 * @property string $multiplayer
 * @property string $developer
 * @property string $img
 * @property string $description
 */
class Game extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'genre', 'release_date', 'multiplayer', 'developer', 'description'], 'required'],
            [['multiplayer', 'description'], 'string'],
            [['name', 'genre', 'release_date', 'developer', 'video'], 'string', 'max' => 255],
            [['img'], 'file', 'extensions'=>'jpg, gif, png'],
//            [['video'], 'file', 'extensions'=>'mp4', 'mimeTypes' => 'video/mp4,video/mpeg'],
            [['img', 'video'], 'required',
                'when' => function($model)
                {return $model->isNewRecord;},
                'whenClient' => "function (attribute, value) {
                    return $('.game-form form').attr('action').indexOf('create') > -1;
                }"
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'genre' => 'Жанр',
            'release_date' => 'Дата виходу',
            'multiplayer' => 'Мультиплеєр',
            'developer' => 'Розробники',
            'img' => 'Зображення',
            'description' => 'Опис',
            'video' => 'video'
        ];
    }

    public function getImage()
    {
        return $this->img;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function uploadImage($oldImage = null) {
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array (you may need to use
        // getInstances method)
        if(isset($oldImage))
            $this->img = $oldImage;

        $image = UploadedFile::getInstance($this, 'img');

        if (empty($image) && !isset($image)) {
            return false;
        }

        $file_name = explode(".", $image->name);
        $ext = end($file_name);

        $path = Yii::$app->params['uploadPathGame'] . $this->name . '.' .$ext;

        $this->img = $path;

        // the uploaded image instance
        return $image;
    }

    public function uploadVideo($oldVideo = null) {
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array (you may need to use
        // getInstances method)
        if(isset($oldVideo))
            $this->video = $oldVideo;

        $video = UploadedFile::getInstance($this, 'video');
        var_dump($video);die;

        if (empty($video) && !isset($video)) {
            return false;
        }

        $file_name = explode(".", $video->name);
        $ext = end($file_name);

        $path = Yii::$app->params['uploadPathGameVideo'] . $this->name . '.' .$ext;

        $this->video = $path;

        // the uploaded video instance
        return $video;
    }

    public function deleteImage() {
        $file = $this->getImage();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        return true;
    }

    public function deleteVideo() {
        $file = $this->getVideo();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        return true;
    }
}
