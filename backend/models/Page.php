<?php

namespace backend\models;


/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $sort
 * @property string $meta_title
 * @property string $meta_desc
 * @property string $meta_key
 * @property string $url
 * @property string $h1
 * @property string $content
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort', 'meta_title', 'meta_desc', 'meta_key', 'url', 'h1', 'content'], 'required'],
            [['sort'], 'integer'],
            [['content'], 'string'],
            [['meta_title', 'meta_desc', 'meta_key', 'url', 'h1'], 'string', 'max' => 255],
            [['show_at_main'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Тип сторінки',
            'sort' => 'Порядок сортування',
            'meta_title' => 'Meta Title',
            'meta_desc' => 'Meta Desc',
            'meta_key' => 'Meta Key',
            'url' => 'Адреса сторінки',
            'h1' => 'Заголовок',
            'content' => 'Контент',
            'show_at_main' => 'Показувати на головній'
        ];
    }
}
