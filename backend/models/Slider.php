<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $path
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path'], 'file', 'extensions'=>'jpg, gif, png'],
//            [['video'], 'file', 'extensions'=>'mp4', 'mimeTypes' => 'video/mp4,video/mpeg'],
            [['path'], 'required',
                'when' => function($model)
                {return $model->isNewRecord;},
                'whenClient' => "function (attribute, value) {
                    return $('.game-form form').attr('action').indexOf('create') > -1;
                }"
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
        ];
    }

    public function getImage()
    {
        return $this->path;
    }

    public function uploadImage($oldImage = null) {
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array (you may need to use
        // getInstances method)
        if(isset($oldImage))
            $this->path = $oldImage;

        $image = UploadedFile::getInstance($this, 'path');

        if (empty($image) && !isset($image)) {
            return false;
        }

        $file_name = explode(".", $image->name);
        $ext = end($file_name);

        $randomString = Yii::$app->getSecurity()->generateRandomString(7);

        $path = Yii::$app->params['uploadPathSlider'] . $randomString . '.' .$ext;

        $this->path = $path;

        // the uploaded image instance
        return $image;
    }

    public function deleteImage() {
        $file = $this->getImage();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        return true;
    }
}
