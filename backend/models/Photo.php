<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "photo".
 *
 * @property integer $id
 * @property string $img
 */
class Photo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['img'], 'required'],
            [['img', 'thumb'], 'file', 'extensions'=>'jpg, gif, png'],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img' => 'Фото',
            'description' => 'Опис фото',
            'thumb' => 'Прев\`ю'
        ];
    }

    public function getImage()
    {
        return $this->img;
    }

    public function getThumb()
    {
        return $this->thumb;
    }

    public function uploadImage($oldImage = null) {
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array (you may need to use
        // getInstances method)
        if(isset($oldImage))
            $this->img = $oldImage;

        $image = UploadedFile::getInstance($this, 'img');

        if (empty($image) && !isset($image)) {
            return false;
        }

//        $file_name = explode(".", $image->name);
//        $ext = end($file_name);
        $ext = $image->getExtension();

        $randomString = Yii::$app->getSecurity()->generateRandomString(7);

        $origin_path = Yii::$app->params['uploadPathPhoto'] . $randomString . '.' .$ext;
        $thumb_path = Yii::$app->params['uploadPathPhotoThumb'] . $randomString . '.' .$ext;

        $this->img = $origin_path;
        $this->thumb = $thumb_path;

        // the uploaded image instance
        return $image;
    }

    public function deleteImage() {
        $file = $this->getImage();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        unlink($this->getThumb());

        return true;
    }
}
