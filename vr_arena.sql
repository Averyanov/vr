-- Adminer 4.0.2 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '+02:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_interval_id` int(11) NOT NULL,
  `station_specimen_id` int(2) NOT NULL,
  `user_ip` int(10) unsigned NOT NULL,
  `price` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `time_interval_id` (`time_interval_id`),
  KEY `station_specimen_id` (`station_specimen_id`),
  CONSTRAINT `booking_ibfk_3` FOREIGN KEY (`time_interval_id`) REFERENCES `time_interval` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `booking_ibfk_4` FOREIGN KEY (`station_specimen_id`) REFERENCES `station_specimen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `booking` (`id`, `time_interval_id`, `station_specimen_id`, `user_ip`, `price`) VALUES
(5,	1,	1,	3221234342,	200),
(6,	2,	1,	3221234342,	200),
(8,	4,	3,	2345,	200),
(9,	5,	2,	25364985,	3762),
(10,	6,	2,	2130706433,	25),
(11,	7,	2,	2130706433,	25),
(12,	8,	1,	2130706433,	25),
(13,	9,	1,	2130706433,	25),
(14,	10,	2,	2130706433,	25),
(15,	11,	1,	2130706433,	25),
(16,	12,	1,	2130706433,	25),
(17,	13,	1,	2130706433,	25),
(18,	14,	1,	2130706433,	25);

DROP TABLE IF EXISTS `Contacts`;
CREATE TABLE `Contacts` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `Contacts` (`id`, `name`, `value`) VALUES
(1,	'address',	'м. Житомир,<br> вул. Черняхівського, 11'),
(2,	'e-mail',	'vrarenaclub@gmail.com'),
(3,	'tel',	'+38 093 186 87 15');

DROP TABLE IF EXISTS `discount`;
CREATE TABLE `discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `discount` (`id`, `name`) VALUES
(1,	'usual'),
(2,	'discount');

DROP TABLE IF EXISTS `game`;
CREATE TABLE `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `genre` varchar(255) NOT NULL,
  `release_date` varchar(255) NOT NULL,
  `multiplayer` enum('yes','no') CHARACTER SET utf32 NOT NULL,
  `developer` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `game` (`id`, `name`, `genre`, `release_date`, `multiplayer`, `developer`, `img`, `video`, `description`) VALUES
(32,	'Final Approach',	'Екшн, Казуальні ігри, Інді, Симулятори, Стратегії',	'5 кві 2016',	'no',	'Phaser Lock Interactive',	'upload/image/game/Final Approach.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/_2at4xZwcDA\" frameborder=\"0\" allowfullscreen></iframe>',	'Візьміть під свій контроль небо, як ніколи раніше в повній віртуальнії реальності світу! Літак у вогні, повітряні зіткнення, аварійні вертолітні місії, військові навчання і багато іншого!'),
(31,	'DiRT Rally',	'Перегони, Симулятори, Спорт',	'7 гру 2015',	'yes',	'Codemasters Racing Studio',	'upload/image/game/DiRT Rally.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/ZMl-yRDiTcY\" frameborder=\"0\" allowfullscreen></iframe>',	'DiRT Rally є найбільш достовірним і захоплюючим симулятором ралі по небезпечним дорогам, знаючи, що одна аварія може завдати непоправної шкоди вашій кар\'єрі.'),
(29,	'Project CARS',	'Перегони, Симулятори, Спорт',	'16 бер 2015',	'yes',	'Slightly Mad Studios',	'upload/image/game/Project CARS.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/GSjJ2TiekDU\" frameborder=\"0\" allowfullscreen></iframe>',	'Перегони на самих різних 4-х колісних транспортних засобах: від невеличких карів до сучасних болідів.'),
(28,	'theBlu',	'Пригодницькі ігри, Казуальні ігри, Інді, Симулятори',	'5 кві 2016',	'no',	'Wevr, Inc.',	'upload/image/game/theBlu.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/wXxsL9cGtXk\" frameborder=\"0\" allowfullscreen></iframe>',	'Відчуйте дивовижність і велич океану відвідавши ряд місць існування підводних мешканців і зустрівшись віч-на-віч з деякими з найбільш неймовірних жителів нашої блакитної планети.'),
(40,	'Everest VR',	'Симулятори',	'2 сер 2016',	'no',	'Sólfar Studios, RVX',	'upload/image/game/Everest VR.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/_EhObeR449Q\" frameborder=\"0\" allowfullscreen></iframe>',	'Приголомшуючий симулятор подоодрожі до вершини Евересту. Ланцюг кемпів, які включаюь різні види випробувань із світу альпінізму, від чого підкорити найвищу точку нашої планети стало ще легше'),
(25,	'Zombie Training Simulator',	'Екшен',	'15 кві 2016',	'no',	'Acceleroto, Inc.',	'upload/image/game/Zombie Training Simulator.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/oLNvYWRX77I\" frameborder=\"0\" allowfullscreen></iframe>',	'Готові ви до зомбі-апокаліпсису? Ми зробили дослідження і хочемо переконатись, що ви готові.'),
(26,	'Tilt Brush',	'Проектування та ілюстрація',	'5 кві 2016',	'no',	'Google',	'upload/image/game/Tilt Brush.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/TckqNdrdbgk\" frameborder=\"0\" allowfullscreen></iframe>',	'Tilt Brush дозволяє малювати в 3D-просторі з віртуальною реальністю. Дайте волю своєї творчості з тривимірними мазками, зірками, світлом і, навіть, вогнем. Ваша кімната -  Ваше полотно. Ваша палітра - Ваша уява. Можливості нескінченні.'),
(24,	'Destinations',	'Казуальні ігри, Free-to-Play, Симулятори',	'9 чер 2016',	'yes',	'Valve',	'upload/image/game/Destinations.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3TPkQReJX-s\" frameborder=\"0\" allowfullscreen></iframe>',	'Destinations дозволяє досліджувати як реальні так і уявні місця у віртуальній реальності з друзями. Відвідати і дізнатися про різні країни, вивчити своє улюблене ігрове середовище, або грати в ігри з іншими гравцями, запросити своїх друзів і відправиться досліджувати!'),
(23,	'Waltz of the Wizard',	'Пригодницькі ігри, Інді, Симулятори',	'31 тра 2016',	'yes',	'Aldin Dynamics',	'upload/image/game/Waltz of the Wizard.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/QXFaJjHcel4\" frameborder=\"0\" allowfullscreen></iframe>',	'Waltz of the Wizard це реальність, що дозволяє відчути себе чаклуном. Об\'єднати загадкові інгредієнти в киплячий котел з допомогою стародавнього духу в пастці людського черепа. Випустити творче або руйнівне чаклунство на повністю інтерактивний віртуальний світ'),
(22,	'Richie\'s Plank Experience',	'Казуальні ігри, Інді, Симулятор',	'20 вер 2016',	'no',	'Toast',	'upload/image/game/Richie\'s Plank Experience.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/MvNTRo_4Qk4\" frameborder=\"0\" allowfullscreen></iframe>',	'Любиш чи боїшся висоти? Ким ти хотів стати в дитинстві - пожежником чи художником? Ця гра задовольнить потреби обох сторін і не залишить байдужих'),
(21,	'Fruit Ninja VR',	'Екшн, Казуальні ігри, Симулятори, Спорт',	'7 лип 2016',	'no',	'Halfbrick Studios Pty Ltd ',	'upload/image/game/Fruit Ninja VR.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/pBBxcC1i1B0\" frameborder=\"0\" allowfullscreen></iframe>',	'Зроби крок назустріч всесвіту Fruit Ninja і випробуй шматочок віртуальної реальності, як ніколи раніше. Зіграй у Fruit Ninja VR зараз!'),
(20,	'Audioshield',	'Інді',	'5 кві 2016',	'no',	'Dylan Fitterer',	'upload/image/game/Audioshield.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/_XAufOZMQaY\" frameborder=\"0\" allowfullscreen></iframe>',	'Блокуй удари! Audioshield ставить Вас в точці кульмінації музичного твору, направляючи звукові кулі. Блокуй кулі своїми щитами в такт музиці. Працює з будь-яким файлом пісні, а також онлайн трансляцією.'),
(19,	'ZenBlade',	' Екшн, Симулятори, Спорт',	'30 кві 2016',	'no',	'Atomic VR Inc.',	'upload/image/game/ZenBlade.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/CeeCZLp34Ac\" frameborder=\"0\" allowfullscreen></iframe>',	'Zen Blade (формально Ninja Trainer) це гра на володіння мечем, розроблений спеціально для VR з контролерами руху. Ця гра для всіх, простота в керуванні та зручний геймплей завоює Вашу увагу на всі 100%'),
(18,	'Trials on Tatooine',	'Екшн',	'18 лип 2016',	'no',	'ILMxLAB',	'upload/image/game/Trials on Tatooine.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/6PkbgNC9UBk\" frameborder=\"0\" allowfullscreen></iframe>',	'Почни свое тренування джедая в цій Star Wars віртуальнії реальності. Тисячолітній Сокіл на ремонті, твоя задача захистити його від нападу з боку імперських штурмовиків з допомогою світлового меча.'),
(17,	'Space Pirate Trainer',	'Екшн, Дочасний доступ',	'5 кві 2016',	'no',	'I-Illusions',	'upload/image/game/Space Pirate Trainer.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/-JC52_ocftw\" frameborder=\"0\" allowfullscreen></iframe>',	'Space Pirate Trainer є офіційним тренером для наслідувачів космічних піратів на HTC Vive. Підніміть ваші бластери, надів кросівки і пританцьовуючи прямуйте в зал слави.'),
(15,	'Serious Sam VR: The Last Hope',	' Екшн, Інді',	'17 жов 2016',	'no',	'Devolver Digital, Croteam',	'upload/image/game/Serious Sam VR: The Last Hope.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/CIVBFMXSGCA\" frameborder=\"0\" allowfullscreen></iframe>',	'Крутий Сем повертається! І на цей раз це реально!'),
(16,	'Snow Fortress',	'Екшн, Казуальні ігри, Інді, Симулятори, Дочасний доступ',	'9 чер 2016',	'yes',	'Mythical City Games',	'upload/image/game/Snow Fortress.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/IvKk7qnSouQ\" frameborder=\"0\" allowfullscreen></iframe>',	'Snow Fortress. Згадайте дитинство, будуючи форти і проводячи епічні війни в VR! Відкривайте нові інструменти для захисту вашої фортеці та способи надокучити вашим супротивникам!'),
(13,	'Raw Data',	'Екшн, Інді, Стратегії, Дочасний доступ',	'15 лип 2016',	'yes',	'Survios',	'upload/image/game/Raw Data.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/O9U8-4AT7fs\" frameborder=\"0\" allowfullscreen></iframe>',	'Побудований з нуля для VR, бойовий геймплей гри, інтуїтивно зрозуміле управління. Кидаючи виклик ворогам у науково-фантастичній атмосфері, занурять вас в сюрреалістичних середовище Eden Corp.'),
(14,	'Realities',	'Пригодницькі ігри, Free-to-Play, Симулятори',	'5 кві 2016',	'no',	'realities.io',	'upload/image/game/Realities.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/wJ7pOePo_wo\" frameborder=\"0\" allowfullscreen></iframe>',	'Одягни гарнітуру віртуальної реальності і поглянь на деякі з найчарівніших місць по всьому світу. Ви можете вибрати місце на земній кулі, і  подорожувати одним натисканням кнопки!'),
(12,	'Quanero',	'Free-to-Play',	'12 сер 2016',	'no',	'LaserBoys3000',	'upload/image/game/Quanero.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/BaWAedT4z1U\" frameborder=\"0\" allowfullscreen></iframe>',	'Quanero експериментальний досвід для В.Р. HTC Vive. Ваша мета полягає в тому, щоб з\'ясувати, що викликало бурхливий інцидент в футуристичному барі, керуючи часом. Ви можете уповільнити час, скасувати або зупинити його. Досліджуйте сцену, розблоковуючи точки розгалуження розповіді та розгадайте таємницю'),
(10,	'Machine Learning: Episode I',	'Екшн, Пригодницькі ігри, Інді',	'10 вер 2016',	'no',	'Singularity Lab ',	'upload/image/game/Machine Learning: Episode I.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/V7lVKfqVeAU\" frameborder=\"0\" allowfullscreen></iframe>',	'Machine Learning: Episode I це перша аркадна головоломка для HTC Vive. Ви граєте як А.І. робот, створений в лабораторії DARPA в найближчому майбутньому. Один з учених тут, щоб перевірити ваше фізичне, сприйняття і пізнавальні навички.'),
(9,	'Lightblade VR',	'Екшн, Інді, Симулятори',	'1 чер 2016',	'no',	'Andreas Hager Gaming ',	'upload/image/game/Lightblade VR.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/IkZIjYhq8fo\" frameborder=\"0\" allowfullscreen></iframe>',	'Lightblade VR це симулятор джедая. Мрія дитинства здійснилася! Отримай підготовку у свого персонального робота та навчись відбивати лазерні промені!'),
(8,	'The lab',	'Free-to-Play',	'5 кві 2016',	'no',	'Valve',	'upload/image/game/The lab.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/RtR-0waVOIA\" frameborder=\"0\" allowfullscreen></iframe>',	'Ласкаво просимо в лабораторію - збірник кімнатного масштабу VR експериментів Valve, до яких входять: інженерна робота, захист замку, механічний друг, і багато іншого.'),
(7,	'Island 359',	'Екшн, Пригодницькі ігри, Інді, Дочасний доступ',	'23 сер 2016',	'no',	'CloudGate Studio, Inc.	',	'upload/image/game/Island 359.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/qKM3HTCeA00\" frameborder=\"0\" allowfullscreen></iframe>',	'Island 359 це віртуальна гра - симулятор виживання для HTC Vive. Гравці будуть використовувати пістолети, ножі, поновлення та інші інструменти, знайдені на острові, щоб вижити так довго, як вони можуть проти орд динозаврів'),
(6,	'Hover Junkers',	'Екшн, Пригодницькі ігри, Інді',	'5 кві. 2016',	'yes',	'Stress Level Zero',	'upload/image/game/Hover Junkers.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/c77a1sOFWcQ\" frameborder=\"0\" allowfullscreen></iframe>',	'Ви зможете взяти участь в багатокористувацьких VR боях, і грати так, як ніколи не грали раніше. Пілот і власноруч зміцнений літальний пристрій повинні вижити в суворій перестрілці'),
(5,	'Budget Cuts Demo',	'Екшен, Пригодницькі ігри, Інді, Стелс',	'5 кві. 2016',	'no',	'Neat Corporation',	'upload/image/game/Budget Cuts Demo.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Q7dVaembmgc\" frameborder=\"0\" allowfullscreen></iframe>',	'Це пре-альфа гри Budget Cuts. Майбутня VR-only гра стелс для HTC Vive, в даний час в розвитку Neat Corporation!'),
(4,	'The Brookhaven Experiment',	'Екшен, Інді',	'5 Лип. 2016',	'no',	'Phosphor Games',	'upload/image/game/The Brookhaven Experiment.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/rO3i3x_EzRA\" frameborder=\"0\" allowfullscreen></iframe>',	'Brookhaven це шутер VR для HTC Vive. Гравці повинні будуть використовувати зброю, інструменти та забезпечення, щоб подолати все страшніші та неймовірніші хвилі жахливих монстрів в спробі з\'ясувати, що викликало початок кінця світу, і, якщо вони достатньо сильні, зупинити його'),
(3,	'HordeZ',	'Екшен',	'29 кві. 2016',	'yes',	'Zenz VR',	'upload/image/game/HordeZ.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/rlst_EQxcIo\" frameborder=\"0\" allowfullscreen></iframe>',	'Якщо ви шукаєте поєднання між класикою, такою як Doom, Left 4 Dead і House of the Dead у VR, тоді Ви її знайшли! Швидкий і простий геймплей, який занурить вас в страшний світі зомбі і демонів'),
(33,	'Elite Dangerous: Arena',	'Екшн, MMO, Рольові ігри, Симулятори',	'16 лют 2016',	'yes',	'Frontier Developments	',	'upload/image/game/Elite Dangerous: Arena.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/cq07zkElouE\" frameborder=\"0\" allowfullscreen></iframe>',	'Приймай учать у тісних космічних боях проти найкращих галактичних пілотів у режимі PVP.'),
(39,	'Google Earth VR',	'Пригодницькі ігри, Free-to-Play, Симулятори',	'16 лис 2016',	'no',	'Google',	'upload/image/game/Google Earth VR.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/SCrkZOx5Q1M\" frameborder=\"0\" allowfullscreen></iframe>',	'Google Earth VR дозволить досліджувати світ у віртуальній реальності. Прогуляйтеся по вулицях Токіо, паріть над Гранд-Каньйон, або пройдіться по Ейфелевій вежі. Цей віртуальний додаток дозволяє побачити міста світу, пам\'ятки і природні чудеса. Ви можете літати над містом або злетіти в космос. Земля VR поставляється з кінематографічними турами які демонструють Вам річки Амазонки, Манхеттен, Гранд-Каньйон, швейцарські Альпах, і багато іншого.'),
(35,	'War Thunder',	'Авіасимулятор',	'15 сер 2013',	'yes',	'Gaijin Entertainment',	'upload/image/game/War Thunder.jpg',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/LMih2KRW8VQ\" frameborder=\"0\" allowfullscreen></iframe>',	'Комп\'ютерна мультиплеєрна онлайн-гра, в якій розробниками було максимально точно зімітовано бойову авіацію часів Другої Світової війни і післявоєнного періоду');

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base',	1476276303),
('m130524_201442_init',	1476276465);

DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `header` text NOT NULL,
  `short_description` text NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `news` (`id`, `date`, `header`, `short_description`, `text`) VALUES
(1,	'2016-11-21 01:23:02',	'Google Earth VR відтепер доступна в Steam!',	'Компанія Google опублікувала свій VR-застосунок для досліджувачів нашої планети в Steam. Можливості Google Earth VR можна переглянути і відео',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/SCrkZOx5Q1M\" frameborder=\"0\" allowfullscreen></iframe>\r\nGoogle Earth VR дозволить досліджувати нашу планету, перебуваючи у віртуальній реальності. Миттева зміна краєвидів: від космічно маштабу до рівня Вашого будинку. Також Google попрацювали на тривимірними моделями міст, природніми спорудами та архітектурними надбаннями. Наш відвідувач має змогу побачити не лише двовимірне зображення міста, а і об\'ємні споруди. Крім того, до швидкого доступу включені тури по самим живописним місцям планети: Ейфелева вежа, Колізей та інші..'),
(2,	'2016-11-21 01:22:52',	'HTC Vive отримає новий бездротовий інтерфейс',	'Однією з самих серйозних проблем сучасної віртуальної реальності є наявність великої кількості дротів, якими користувач \"прикутий\" до заліза. Звісно, вони заважають вільному пересуванню. До цього часу одним із самих вдалих рішень були рюкзаки з апаратним забезпеченням та батареєю))... але компанія TPCAST запропонувала інший спосіб позбутись \"дротової залежності\"',	'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/vYMzuvjBbN4\" frameborder=\"0\" allowfullscreen></iframe>\r\n<br>\r\nЦе спеціальний пристрій, який буде передавати зображення для шолома HTC Vive без дротового підключення. Пристрій складається з двох модулів і кріплення до гарнітури. Один з модулів відповідає безпосередньо за передачу даних, а інший містить батарею. Акумулятора повинно вистачати на півтори години безперервної роботи. Очікується продаж версії TPCAST зі збільшеню батареєю, яку доведеться покласти в кишеню. Розробники обіцяють, що бездротовий режим не стане причиню затримок або погіршення якості зображення.\r\nВ продаж пристрій TPCAST надійде на початку 2017 року за ціною $220. Можливість зробити передзамовлення вже доступна, при чому приорітет отримують ті користувачі, які вже купили гарнітуру HTC Vive');

DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(63) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `newsletter` (`id`, `email`) VALUES
(1,	'some@some.com'),
(3,	'some@some.com1'),
(4,	'some@com.com'),
(5,	'some@some.com3'),
(6,	'1@1.1'),
(7,	'1@1.2'),
(8,	'bla@some.com'),
(9,	'1@1.ua'),
(10,	'Some@some.co'),
(11,	'bla@1.1');

DROP TABLE IF EXISTS `page`;
CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` tinyint(4) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_desc` varchar(255) NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `h1` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `show_at_main` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `page` (`id`, `sort`, `meta_title`, `meta_desc`, `meta_key`, `url`, `h1`, `content`, `show_at_main`) VALUES
(3,	1,	'Ігри',	'cvb',	'xcvb',	'games',	'Ігри',	'game-content\r\n',	1),
(4,	2,	'Ціни',	'price',	'virtual rality',	'price',	'Ціни',	'<table class=\"price\">\r\n                    <tr>\r\n                        <th>Тривалість сеансу,хв.</th>\r\n                        <th>Звичайна вартість грн.</th>\r\n                        <th>Знижка</th>\r\n                        <th>Вартість сеансу <br> Oculus Rift CV1 та  HTC Vive <br> в будній день до 17:00 <br><span class=\"red\">(будній день після 17:00 <br> та у вихідний)</span>, грн.</th>\r\n                    </tr>\r\n                    <tr>\r\n                        <th>15</th>\r\n                        <td><del>35 <span class=\"red\">(60)</span></del></td>\r\n                        <td>40-50 %</td>\r\n                        <td>25 <span class=\"red\">(40)</span></td>\r\n                    </tr>\r\n                    <tr>\r\n                        <th>30</th>\r\n                        <td><del>60<span class=\"red\">(90)</span></del></td>\r\n                        <td>20-50 %</td>\r\n                        <td>50 <span class=\"red\">(60)</span></td>\r\n                    </tr>\r\n                    <tr>\r\n                        <th>60 і більше</th>\r\n                        <td><del>100 <span class=\"red\">(150)</span></del></td>\r\n                        <td>20-50 %</td>\r\n                        <td>80 <span class=\"red\">(100)</span></td>\r\n                    </tr>\r\n                </table>\r\n                <br>\r\n<div class=\"span5 price\">\r\n                <ul class=\"rule\">\r\n                    <li>Мінімальна тривалість сеансу 15 хв.</li>\r\n                    <li>Оплата проводиться наперед.</li>\r\n                    <li>Перед сеансом гравець отримує до 5 хв. додаткового часу на інструктаж (проводиться працівником клубу).</li>\r\n                    <li>Якщо ти прийшов з друзями і хочеш з ними поділися грою під час сеансу, інструктаж проводиться комусь одному (якщо ж треба комусь пояснити ще раз, гаразд, але додаткових 5 хв не надається).</li>\r\n                    <li>Якщо гра перервалася з технічних причин, втрачений час додається до сеансу.</li>\r\n                    <li>При відмові клієнта починати гру <span class=\"red\">під час інструктажу</span> гроші за сеанс можуть бути повернуті.</li>\r\n                    <li>Якщо ти вирішив перервати гру раніше ніж закічиться сеанс, гроші за залишок часу не повертаються.</li>\r\n                </ul>\r\n</div>\r\n<div class=\"span5 price\">\r\n                <br>\r\n                Замовити час можна за тел: +38 093 186 87 15\r\n                <br>\r\n                або надіславши запит на електронну адресу\r\n                <br>\r\n                reserve@vr-arena.club\r\n                <br>\r\n                Також Ви можете замовити клуб віртуальної реальності на всю ніч (з 22:00 до 6:00).\r\n                <br>\r\n                <br>\r\n                Вартість послуги - 1000 грн\r\n                <br>\r\n                <br>\r\n                Крім усього іншого до переліку послуг входять безкоштовні кава та чай.\r\n</div>',	1),
(5,	2,	'Фото',	'фото',	'фото',	'photo',	'Фото',	'gallery-content',	0),
(6,	4,	'Бронювання',	'desv',	'key',	'booking',	'Бронювання',	'1',	1),
(7,	5,	'Контакти',	'деск',	'кей',	'contacts',	'Контакти',	'1',	0),
(8,	8,	'Новини',	'Новини',	'новини, вр, vr, житомир',	'news',	'Новини',	'1',	0),
(9,	8,	'﻿Як працює шолом віртуальної реальності?',	'VR, житомир',	'Житомир віртуальна реальність',	'what_is_it',	'Що це таке?',	'<div class=\"what_is_it\"><p>Якщо не вдаватись в подробиці, то все достатньо просто.</p>\r\n<p>Кожен шолом має свої особливості роботи, але загальний принцип один - в шоломі розміщені датчики руху, до їх числа відносяться: гіроскоп, акселерометр, компас, додаткові камери та інші, загальною кількістю 32 шт</p>\r\n\r\n<p>Усі вони виконують функції орієнтування в просторі, реєструючи зміну кута нахилу Вашої голови та положення тіла, віддаючи нові дані для обчислювальної техніки, яка перемальовує зображення в Ваших окулярах. Все це потрібно встигнути зробити протягом 50 мс (це 1/20 секунди), значний шмат роботи, чи не так? Крім того, потрібно пам\'ятати, що людину обладнано кількома очима, двома, якщо я не помиляюсь. Саме така кількість очей дозволить визначити відстань до об\'єкта. Ви навряд над цим задумувались, але Ваш мозок щомиті це пам\'ятає і робить обрахунок відстані, до речі, з вухами та сама історія. Тому обманути мозок стає на порядок важче, але і це нам під силу - шолом обладнано одночасно двома дисплеями, що і робить картинку об\'ємною. Тому за 50 мс нам потрібно двічі обрахувати зображення з різних ракурсів. Якщо не вкластись в цей інтервал, то людина відчує на собі симптоми \"кібер-хвороби\", на практиці дуже схоже на звичайну морську хворобу, за винятком того, що Ви не зможете покласти усю вину на свій вестибулярний апарат і допомогти собі алкоголем, як заведено на морських суднах. Вперше термін сформулював Джонатан Штойер і причина дуже проста - якщо ПО не відреагувало на рух Вашого тіла швидше ніж за 50 мс(сучасне обладнання для окулярів вже вкладається у 30 мс) і перерендерити(перемалювати) зображення, то Ваш мозок відчує обман і просто почне бунт, що і призводить до неприємних відчуттів. Якщо ж Ви прийшли до нас і після кількох хвилин гри стали відчувати прояви \"кібер-хвороби\", то прийміть вітання, Ви - щасливий власник \"реакції мангуста\"\r\n<br>\r\n<p>Від дива природи знову повернемось до чудес наукового прогресу. Та ж доля очікує і джойстики в Ваших руках, там ті самі датчики, той же процес обрахунків за виключення того, що повертається йому лише сигнал для ввімкнення вібродвигуна, який вдало імітує віддачу при пострілах, інфорує про перебування Ваших рук там, де \"не треба\" та ін... Через те, що джойстику так мало потрібно, його вдалось зробити бездротовим, що донедавна не вдавалось зробити з шоломом, але прогрес не стоїть на місці. <a href=\"/news/2\" target =\"_blank\">Стаття для ознайомлення</a>.</p>\r\n<br>\r\n<p>Сподіваюсь, що цієї інформації було достатньо для Вашого допитливого розуму і Ви обов\'язково нас відвідаєте щоб відчути враження від найдивовижнішого обману Ваших органів чуття, наукового прогресу та фантазії провідних геймдевів цього світу.</p></div>',	0);

DROP TABLE IF EXISTS `photo`;
CREATE TABLE `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `photo` (`id`, `img`, `thumb`, `description`) VALUES
(10,	'upload/image/photo/uUfuOzA.jpg',	'upload/image/photo/thumb/uUfuOzA.jpg',	'VR - це майбутнє'),
(9,	'upload/image/photo/tX1Yvwj.jpg',	'upload/image/photo/thumb/tX1Yvwj.jpg',	''),
(12,	'upload/image/photo/dk-Eidz.jpg',	'upload/image/photo/thumb/dk-Eidz.jpg',	'The Lab розроблено спеціально для новачків'),
(13,	'upload/image/photo/i4lrQah.jpg',	'upload/image/photo/thumb/i4lrQah.jpg',	''),
(15,	'upload/image/photo/cIi7lSr.jpg',	'upload/image/photo/thumb/cIi7lSr.jpg',	'Симулятор справжніх перегонів'),
(16,	'upload/image/photo/rCB8cGI.jpg',	'upload/image/photo/thumb/rCB8cGI.jpg',	''),
(18,	'upload/image/photo/X6bfPTd.jpg',	'upload/image/photo/thumb/X6bfPTd.jpg',	''),
(20,	'upload/image/photo/gbaIUVq.jpg',	'upload/image/photo/thumb/gbaIUVq.jpg',	''),
(24,	'upload/image/photo/4j4237H.jpg',	'upload/image/photo/thumb/4j4237H.jpg',	''),
(23,	'upload/image/photo/z659Daz.jpg',	'upload/image/photo/thumb/z659Daz.jpg',	''),
(25,	'upload/image/photo/r330204.jpg',	'upload/image/photo/thumb/r330204.jpg',	'');

DROP TABLE IF EXISTS `price`;
CREATE TABLE `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_id` int(11) NOT NULL,
  `mins` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `discount_id` (`discount_id`),
  CONSTRAINT `price_ibfk_1` FOREIGN KEY (`discount_id`) REFERENCES `discount` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `price` (`id`, `discount_id`, `mins`, `price`) VALUES
(1,	1,	15,	40),
(2,	1,	30,	60),
(3,	1,	45,	90),
(4,	1,	60,	100),
(5,	2,	15,	25),
(6,	2,	30,	50),
(7,	2,	45,	75),
(8,	2,	60,	80);

DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `slider` (`id`, `path`) VALUES
(6,	'upload/image/slider/5GdzFA8.jpg'),
(7,	'upload/image/slider/GeUdrhO.jpg'),
(8,	'upload/image/slider/85thVbo.jpg'),
(9,	'upload/image/slider/O7aABzW.jpg');

DROP TABLE IF EXISTS `station_specimen`;
CREATE TABLE `station_specimen` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `station_type_id` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `station_type_id` (`station_type_id`),
  CONSTRAINT `station_specimen_ibfk_1` FOREIGN KEY (`station_type_id`) REFERENCES `station_type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `station_specimen` (`id`, `station_type_id`) VALUES
(1,	1),
(2,	1),
(3,	2);

DROP TABLE IF EXISTS `station_type`;
CREATE TABLE `station_type` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `station_type` (`id`, `name`) VALUES
(1,	'HTC Vive'),
(2,	'Oculus Rift CV1');

DROP TABLE IF EXISTS `time_interval`;
CREATE TABLE `time_interval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `begin` datetime NOT NULL,
  `end` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `time_interval` (`id`, `begin`, `end`) VALUES
(1,	'2017-02-12 10:50:00',	'2017-02-12 14:05:00'),
(2,	'2017-02-12 14:30:00',	'2017-02-12 16:30:00'),
(4,	'2017-02-12 10:00:00',	'2017-02-12 11:00:00'),
(5,	'2017-02-12 10:00:00',	'2017-02-12 11:00:00'),
(6,	'2017-02-22 14:12:00',	'2017-02-22 14:27:00'),
(7,	'2017-02-22 12:26:00',	'2017-02-22 12:41:00'),
(8,	'2017-02-22 13:14:00',	'2017-02-22 13:29:00'),
(9,	'2017-02-22 15:02:00',	'2017-02-22 15:17:00'),
(10,	'2017-02-22 15:52:00',	'2017-02-22 16:07:00'),
(11,	'2017-02-22 16:46:00',	'2017-02-22 17:01:00'),
(12,	'2017-02-22 10:42:00',	'2017-02-22 10:57:00'),
(13,	'2017-02-23 13:44:00',	'2017-02-23 13:59:00'),
(14,	'2017-03-01 13:22:00',	'2017-03-01 13:37:00');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(4,	'some',	'ax0UQHxI6XKLlDB57ON3M8luBAtvjRlM',	'$2y$13$b7qvmP3xloSe5pcHbi91CO6BO//EgFUgpZ44F0nd0zY8/CCsNMsNm',	NULL,	'some@some.com',	10,	1486861869,	1486861869);

-- 2018-03-14 01:42:35
